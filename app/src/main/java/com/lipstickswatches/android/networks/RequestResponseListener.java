package com.lipstickswatches.android.networks;

/**
 * Created by Awais Nasir Toor on 12/7/2015.
 */


public interface RequestResponseListener<T> {
    void onResult(Integer response, T object);
}


