package com.lipstickswatches.android.networks;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.lipstickswatches.android.pojo.BaseResponse;
import com.lipstickswatches.android.utils.Constants;

import java.util.Map;

public class NetworkManager {

    public static NetworkManager instance = null;

    public RequestQueue requestQueue;

    private NetworkManager(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized NetworkManager getInstance(Context context) {
        if (null == instance)
            instance = new NetworkManager(context);
        return instance;
    }

    //this is so you don't need to pass context each time
    public static synchronized NetworkManager getInstance() {
        if (null == instance) {
            throw new IllegalStateException(NetworkManager.class.getSimpleName() +
                    " is not initialized, call getInstance(...) first");
        }
        return instance;
    }


    public void get(String tag, final RequestResponseListener<Object> listener) {
        final GsonRequest<BaseResponse> request = new GsonRequest<>(Request.Method.GET,
                Constants.getBaseUrl() + tag,
                BaseResponse.class,
                null,
                new Response.Listener<BaseResponse>() {
                    @Override
                    public void onResponse(BaseResponse response) {
//                        Log.e("NETWORK", "Code: " + response.getCode() + ", Msg:" + response.getMessage());
                        switch (response.getCode()) {
                            case Constants.WebApi.Response.SUCCESS:
                                Gson gson = new Gson();
                                String obj = gson.toJson(response.getData());
//                                Log.e("NETWORK", "DATA: " + obj);
                                listener.onResult(Constants.WebApi.Response.SUCCESS, obj);
                                break;
                            case Constants.WebApi.Response.NOT_FOUND:
                                listener.onResult(Constants.WebApi.Response.NOT_FOUND, null);
                                break;
                            case Constants.WebApi.Response.USERNAME_ALREADY_EXISTS:
                                listener.onResult(Constants.WebApi.Response.USERNAME_ALREADY_EXISTS, null);
                                break;
                            case Constants.WebApi.Response.EMAIL_ALREADY_EXISTS:
                                listener.onResult(Constants.WebApi.Response.EMAIL_ALREADY_EXISTS, null);
                                break;
                            case Constants.WebApi.Response.UNAUTHORIZED:
                                listener.onResult(Constants.WebApi.Response.UNAUTHORIZED, null);
                                break;
                        }

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            listener.onResult(Constants.WebApi.Response.TIMEOUT, null);
                        } else {
                            listener.onResult(Constants.WebApi.Response.NO_INTERNET, null);
                        }

                    }
                }

        );


        RetryPolicy policy = new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        request.setTag(Constants.getBaseUrl() + tag);
        requestQueue.add(request);
    }

    public void post(String tag, Map<String, String> params, final RequestResponseListener<Object> listener) {
        GsonRequest<BaseResponse> request = new GsonRequest<>(Request.Method.POST,
                Constants.getBaseUrl() + tag,
                BaseResponse.class,
                null, params,
                new Response.Listener<BaseResponse>() {
                    @Override
                    public void onResponse(BaseResponse response) {
//                        Log.e("NETWORK", "Code: " + response.getCode() + ", Msg:" + response.getMessage());
                        switch (response.getCode()) {
                            case Constants.WebApi.Response.SUCCESS:
                                Gson gson = new Gson();
                                String obj = gson.toJson(response.getData());
//                                Log.e("NETWORK", "Code: " + response.getCode() + ", Data:" + obj);
                                listener.onResult(Constants.WebApi.Response.SUCCESS, obj);
                                break;
                            case Constants.WebApi.Response.NOT_FOUND:
                                listener.onResult(Constants.WebApi.Response.NOT_FOUND, null);
                                break;
                            case Constants.WebApi.Response.USERNAME_ALREADY_EXISTS:
                                listener.onResult(Constants.WebApi.Response.USERNAME_ALREADY_EXISTS, null);
                                break;
                            case Constants.WebApi.Response.EMAIL_ALREADY_EXISTS:
                                listener.onResult(Constants.WebApi.Response.EMAIL_ALREADY_EXISTS, null);
                                break;
                            case Constants.WebApi.Response.UNAUTHORIZED:
                                listener.onResult(Constants.WebApi.Response.UNAUTHORIZED, null);
                                break;
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Log.e("NETWORK", "Error: " + error.getMessage());
                        if (error.getClass().equals(TimeoutError.class)) {
                            listener.onResult(Constants.WebApi.Response.TIMEOUT, null);
                        } else {
                            listener.onResult(Constants.WebApi.Response.NO_INTERNET, null);
                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        request.setTag(Constants.getBaseUrl() + tag);
        requestQueue.add(request);
    }


}
