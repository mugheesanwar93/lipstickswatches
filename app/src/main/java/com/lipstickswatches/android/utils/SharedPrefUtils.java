package com.lipstickswatches.android.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Imtnan on 06/12/2017.
 * This class is Shared preference Utility used for Storage purposes
 */

public class SharedPrefUtils {
    private static SharedPreferences mSharedPref;

    /**
     * This function initializes shared preference object if required
     *
     * @param context Application context
     */
    public static void init(Context context) {
        if (mSharedPref == null)
            mSharedPref = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
    }

    /**
     * This function returns the String value against provided key
     *
     * @param key      Key against which value is required
     * @param defValue Default value to be returned if no value is found
     * @return The string value against key provided
     */
    public static String readString(String key, String defValue) {
        if (mSharedPref != null)
            return mSharedPref.getString(key, defValue);
        return defValue;
    }

    /**
     * This function put the value against the key provided
     *
     * @param key   Against which we will save the value
     * @param value String Value to be saved
     */
    public static void writeString(String key, String value) {
        if (mSharedPref != null) {
            SharedPreferences.Editor prefsEditor = mSharedPref.edit();
            prefsEditor.putString(key, value);
            prefsEditor.apply();
        }
    }

    /**
     * This function returns the boolean value against provided key
     *
     * @param key      Key against which value is required
     * @param defValue Default value to be returned if no value is found
     * @return The boolean value against key provided
     */
    public static boolean readBoolean(String key, boolean defValue) {
        if (mSharedPref != null)
            return mSharedPref.getBoolean(key, defValue);
        return defValue;
    }

    /**
     * This function put the value against the key provided
     *
     * @param key   Against which we will save the value
     * @param value boolean Value to be saved
     */
    public static void writeBoolean(String key, boolean value) {
        if (mSharedPref != null) {
            SharedPreferences.Editor prefsEditor = mSharedPref.edit();
            prefsEditor.putBoolean(key, value);
            prefsEditor.apply();
        }
    }

    /**
     * This function returns the Integer value against provided key
     *
     * @param key      Key against which value is required
     * @param defValue Default value to be returned if no value is found
     * @return The Integer value against key provided
     */
    public static Integer readInteger(String key, int defValue) {
        if (mSharedPref != null)
            return mSharedPref.getInt(key, defValue);
        return defValue;
    }

    /**
     * This function put the value against the key provided
     *
     * @param key   Against which we will save the value
     * @param value Integer Value to be saved
     */
    public static void writeInteger(String key, Integer value) {
        if (mSharedPref != null) {
            SharedPreferences.Editor prefsEditor = mSharedPref.edit();
            prefsEditor.putInt(key, value).apply();
        }
    }

    /**
     * This function clear all the shared preferences stored
     */
    public static void removeAll() {
        if (mSharedPref != null) {
            SharedPreferences.Editor prefsEditor = mSharedPref.edit();
            prefsEditor.clear().apply();
        }
    }

    public static void remove(String key) {
        if (mSharedPref != null) {
            SharedPreferences.Editor prefsEditor = mSharedPref.edit();
            prefsEditor.remove(key);
        }
    }


}
