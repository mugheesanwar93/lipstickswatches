package com.lipstickswatches.android.utils;

import androidx.room.Room;
import android.content.Context;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.google.android.gms.ads.MobileAds;
import com.lipstickswatches.android.db.AppDatabase;
import com.lipstickswatches.android.networks.NetworkManager;

/**
 * Created by mugheesanwar on 20/09/15.
 */
//@AcraMailSender(mailTo = "awaistoor@gmail.com")
//@AcraToast(length = Toast.LENGTH_LONG,resText = R.string.crash_toast_text)
public class ApplicationController extends MultiDexApplication {
    private static ApplicationController sInstance;
    private static AppDatabase db;

    public static synchronized ApplicationController getInstance() {
        return sInstance;
    }

    public static AppDatabase getDatabaseObj() {
        return db;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
//        ACRA.init(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        NetworkManager.getInstance(this);
        sInstance = this;
        MobileAds.initialize(this, "ca-app-pub-4342164319490409~3048156087");
        db = Room.databaseBuilder(this, AppDatabase.class, "production")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build(); //creating the database instance
    }

}
