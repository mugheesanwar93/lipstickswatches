package com.lipstickswatches.android.utils;

/**
 * Created by mugheesanwar on 15/11/2016.
 */

public class Constants {

    public static String imageUrl = "http://admin.lipstickswatches.com/";

    public static String url = "http://admin.lipstickswatches.com/webapi/";

    public static String sp = "LS";

    public static String getBaseUrl() {
        return url + "v1/";
    }

    public static class WebApi {
        public static class Response {
            public static final int NOT_FOUND = 404;
            public static final int SUCCESS = 200;
            public static final int UNAUTHORIZED = 401;
            public static final int NO_INTERNET = 504;
            public static final int TIMEOUT = 408;
            public static final int USERNAME_ALREADY_EXISTS = 407;
            public static final int EMAIL_ALREADY_EXISTS = 406;
        }
    }

    public static final int MAIN_LIST = 1;
    public static final int TRENDING_LIST = 2;

    public static class AppIntents {
        public static final String ACTION_OPEN_LIPSTICKFRAGMENT = "com.lipstickswatches.android.ACTION_OPEN_LIPSTICKFRAGMENT";
        public static final String ACTION_REFRESH_MAIN_LISTS = "com.lipstickswatches.android.ACTION_REFRESH_MAIN_LISTS";
        public static final String ACTION_REFRESH_TRENDING_LISTS = "com.lipstickswatches.android.ACTION_REFRESH_TRENDING_LISTS";
    }

}
