package com.lipstickswatches.android.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import com.google.android.material.snackbar.Snackbar;
import androidx.loader.content.CursorLoader;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.db.pojo.Brands;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.db.pojo.Types;
import com.lipstickswatches.android.pojo.ProductResponse.Brand;
import com.lipstickswatches.android.pojo.ProductResponse.Datum;
import com.lipstickswatches.android.pojo.ProductResponse.Type;
import com.lipstickswatches.android.pojo.User;
import com.lipstickswatches.android.ui.BaseActivitiy;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by mugheesanwar on 15/11/2016.
 */

public class AppUtil extends BaseActivitiy {

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showSnackBar(View view, String msg) {
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show();
    }

    public static boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static void shareContent(String text, Context context) {
        Intent sharingIntent = new Intent();
        sharingIntent.setAction(Intent.ACTION_SEND);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, text);
        sharingIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sharingIntent, context.getString(R.string.app_name)));
    }

    public static void shareImage(String url, Context context) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("Image/*");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Hey guys check this out!");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(url));
        context.startActivity(Intent.createChooser(sharingIntent, context.getString(R.string.app_name)));
        SharedPrefUtils.remove("save_img_link");
    }

    public static void invitePeople(Context context) {
        Intent sharingIntent = new Intent();
        sharingIntent.setAction(Intent.ACTION_SEND);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Download Lipstickswatches App from Playstore\n" +
                "http://play.google.com/store/apps/details?id=" + context.getPackageName());
        sharingIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sharingIntent, context.getString(R.string.invite_friends)));
    }

    public static void clearSharedPref(Context mContext) {
        SharedPreferences sp = mContext.getSharedPreferences(Constants.sp, Context.MODE_PRIVATE);
        SharedPreferences.Editor et = sp.edit();
        et.clear().apply();
    }

    public static void saveUserSession(User userData) {
        //new class
        SharedPrefUtils.writeInteger("loginState", 1);
        SharedPrefUtils.writeInteger("userId", userData.getId());
        SharedPrefUtils.writeString("userEmail", userData.getEmail());
        SharedPrefUtils.writeString("name", userData.getName());
        SharedPrefUtils.writeString("image", userData.getImageUrl());
        SharedPrefUtils.writeString("status", userData.getStatus());
        SharedPrefUtils.writeString("gender", userData.getGender());
        SharedPrefUtils.writeString("birthday", userData.getBirthday());
    }

    public static void openLink(String url, Context mContext) {
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mContext.startActivity(browserIntent);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    public static String getRealPathFromURI(Context ctx, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};

        CursorLoader cursorLoader = new CursorLoader(
                ctx,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        int column_index =
                cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static String getProductIdFromName(String name) {
        String[] separated = name.split("_");
        String[] finalValue = separated[3].split("\\.");
        return finalValue[0];
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static File saveImage(Bitmap bitmap) throws IOException {
        File file = null;
        if (bitmap != null) {
            File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/LipstickSwatches", "Original Images");
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                } else {

                    new File(mediaStorageDir.getPath(), ".nomedia").createNewFile();

                }
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

            file = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".JPEG");
            try {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(file); //here is set your file path where you want to save or also here you can set file object directly
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream); // bitmap is your Bitmap instance, if you want to compress it you can compress reduce percentage
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.flush();
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    public static int getDisplayWidth(Activity context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int height = displayMetrics.heightPixels;
        return displayMetrics.widthPixels;

    }

    public static void saveProducts(ArrayList<Datum> productList) {
        ArrayList<Products> products = new ArrayList<>();
        for (int i = 0; i < productList.size(); i++) {
            products.add(new Products(
                    productList.get(i).getProduct().getId(),
                    productList.get(i).getProduct().getName(),
                    productList.get(i).getProduct().getColorName(),
                    productList.get(i).getProduct().getColorCode(),
                    productList.get(i).getProduct().getStatus().getId(),
                    productList.get(i).getProduct().getDownloadLink(),
                    productList.get(i).getProduct().getDesignerId(),
                    productList.get(i).getProduct().getBrandId(),
                    productList.get(i).getProduct().getTypeId(),
                    productList.get(i).getProduct().getIsDiscount(),
                    productList.get(i).getProduct().getDiscountCode(),
                    productList.get(i).getProduct().getFavoritedCount(),
                    productList.get(i).getProduct().getDesigner().getName(),
                    productList.get(i).getProduct().getBrand().getName(),
                    productList.get(i).getProduct().getType().getName(),
                    productList.get(i).getProduct().getIsFavoriteByUser(),
                    productList.get(i).getProduct().getCreatedAt(),
                    productList.get(i).getProduct().getUpdatedAt()
            ));
        }

        ApplicationController.getDatabaseObj().productDAO().insertMultipleListRecord(products);
    }

    public static void removeProducts() {
        ApplicationController.getDatabaseObj().productDAO().delete();
    }

    public static void saveBrands(ArrayList<Brand> brandsList) {
        ArrayList<Brands> brands = new ArrayList<>();
        for (int x = 0; x < brandsList.size(); x++) {
            brands.add(new Brands(
                    brandsList.get(x).getId(),
                    brandsList.get(x).getName(),
                    brandsList.get(x).getStatus()
            ));
        }
        ApplicationController.getDatabaseObj().brandsDAO().insertBrands(brands);
    }

    public static void removeBrands() {
        ApplicationController.getDatabaseObj().brandsDAO().deleteBrands();
    }

    public static void saveTypes(ArrayList<Type> typesList) {
        ArrayList<Types> types = new ArrayList<>();
        for (int x = 0; x < typesList.size(); x++) {
            types.add(new Types(
                    typesList.get(x).getId(),
                    typesList.get(x).getName(),
                    typesList.get(x).getStatus()
            ));
        }
        ApplicationController.getDatabaseObj().typesDAO().insertTypes(types);
    }

    public static void removeTypes() {
        ApplicationController.getDatabaseObj().typesDAO().deleteTypes();
    }

    public static void removeLocalData() {
        removeProducts();
        removeBrands();
        removeTypes();
    }

    public static Bitmap addWatermark(Resources res, Bitmap source) {
        int w, h;
        Canvas c;
        Paint paint;
        Bitmap bmp, watermark;
        Matrix matrix;
        float scale;
        RectF r;
        w = source.getWidth();
        h = source.getHeight();
        bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG | Paint.FILTER_BITMAP_FLAG);
        c = new Canvas(bmp);
        c.drawBitmap(source, 0, 0, paint);
        watermark = BitmapFactory.decodeResource(res, R.drawable.ic_logo_toolbar);
        scale = (float) (((float) h * 0.20) / (float) watermark.getHeight());
        matrix = new Matrix();
        matrix.postScale(scale, scale);
        r = new RectF(0, 0, watermark.getWidth(), watermark.getHeight());
        matrix.mapRect(r);
        matrix.postTranslate(w - r.width(), h - r.height());
        c.drawBitmap(watermark, matrix, paint);
        watermark.recycle();
        return bmp;
    }

    public static void changeToolbarFont(Context ctx, TextView toolbar) {
        AssetManager am = ctx.getAssets();
        Typeface typeface = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "header_font.ttf"));
        toolbar.setTypeface(typeface);
    }
}


