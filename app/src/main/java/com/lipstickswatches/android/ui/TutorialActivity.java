package com.lipstickswatches.android.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.viewpager.widget.ViewPager;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.adapter.SliderViewpagerAdapter;

public class TutorialActivity extends BaseActivitiy implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private ViewPager viewPager;
    private Button btnSkip, btnNext;
    private SliderViewpagerAdapter adapter;
    private boolean isFromSettings = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        init();
    }

    private void init() {
        isFromSettings = getIntent().getBooleanExtra("is_from_settings", false);
        viewPager = findViewById(R.id.viewpager);
        btnSkip = findViewById(R.id.btn_skip);
        btnNext = findViewById(R.id.btn_next);
        adapter = new SliderViewpagerAdapter(this);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
        btnSkip.setOnClickListener(this);
        btnNext.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_skip:
                finish();
                if (!isFromSettings) {
                    transitionActivities(TutorialActivity.this, MainContainerActivity.class, true);
                }
                break;
            case R.id.btn_next:
                if (viewPager.getCurrentItem() >= adapter.getCount() - 1) {
                    finish();
                    if (!isFromSettings) {
                        transitionActivities(TutorialActivity.this, MainContainerActivity.class, true);
                    }
                } else {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                }
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (position == adapter.getCount() - 1) {
            btnNext.setText("DONE");
            btnSkip.setVisibility(View.GONE);
        } else {
            btnNext.setText("Next");
            btnSkip.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
