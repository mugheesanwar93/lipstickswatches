package com.lipstickswatches.android.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.adapter.LipstickSwatchesAdapter;
import com.lipstickswatches.android.pojo.GalleryImages;
import com.lipstickswatches.android.utils.AppUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class LipstickSwatchesActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView rvLipstickSwatches;
    private LipstickSwatchesAdapter lipstickSwatchesAdapter;

    private ArrayList<GalleryImages> imagePaths;

    private ImageButton ibClose, ibDelete;
    private TextView toolbar;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lipstick_swatches);
        init();
    }

    private void init() {
        setupToolbar();
        rvLipstickSwatches = findViewById(R.id.rvLipstickSwatches);
//        rvLipstickSwatches.setHasFixedSize(true);
//        GridLayoutManager llm = new GridLayoutManager(this, 2);
        StaggeredGridLayoutManager sLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        rvLipstickSwatches.setLayoutManager(sLayoutManager);
        new GetFilesTask().execute();
        loadAds();

    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setText(getString(R.string.lipstick_swatches));

        ibClose = findViewById(R.id.ibClose);
        ibClose.setOnClickListener(this);
        ibDelete = findViewById(R.id.ibDelete);
        ibDelete.setOnClickListener(this);
        AppUtil.changeToolbarFont(this, toolbar);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            if (resultCode == RESULT_OK) {
                new GetFilesTask().execute();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibClose:
                finish();
                break;
            case R.id.ibDelete:
                if (lipstickSwatchesAdapter != null) {
                    if (lipstickSwatchesAdapter.getSelectedImages().size() > 0) {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.MyDialogTheme);
                        dialog.setTitle("Delete Pictures");
                        dialog.setMessage("Remove (" + lipstickSwatchesAdapter.getSelectedImages().size() + ") pictures from Swatche Factory?");
                        dialog.setNegativeButton("No", null);
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new DeleteFilesTask().execute();
                            }
                        });
                        dialog.setCancelable(true);
                        dialog.show();
                    }
                }

                break;
        }
    }

    public void showDeleteButton() {
        ibDelete.setVisibility(View.VISIBLE);
    }

    public void hideDeleteButton() {
        ibDelete.setVisibility(View.GONE);
    }

    private class GetFilesTask extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(LipstickSwatchesActivity.this, null, getResources().getString(R.string.loading));
            imagePaths = new ArrayList<>();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            File file = new File(android.os.Environment.getExternalStorageDirectory(), "LipstickSwatches");
            String[] okFileExtensions = new String[]{"jpg", "png", "gif", "jpeg"};

            if (file.isDirectory()) {
                File[] listFile = file.listFiles();
                if (listFile != null) {
                    for (File aListFile : listFile) {
                        for (String extension : okFileExtensions)
                            if (aListFile.getName().toLowerCase().endsWith(extension)) {
                                imagePaths.add(new GalleryImages(aListFile.getAbsolutePath(), false));
                            }
                    }
                    return imagePaths.size() > 0;
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dialog.dismiss();
            if (aBoolean) {
                Collections.reverse(imagePaths);
                lipstickSwatchesAdapter = new LipstickSwatchesAdapter(LipstickSwatchesActivity.this, imagePaths);
                rvLipstickSwatches.setAdapter(lipstickSwatchesAdapter);
            } else {
                lipstickSwatchesAdapter = new LipstickSwatchesAdapter(LipstickSwatchesActivity.this, imagePaths);
                rvLipstickSwatches.setAdapter(lipstickSwatchesAdapter);
                lipstickSwatchesAdapter.notifyDataSetChanged();
                AppUtil.showToast(LipstickSwatchesActivity.this, getString(R.string.no_swatches));
            }
        }
    }

    private class DeleteFilesTask extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(LipstickSwatchesActivity.this, null, getResources().getString(R.string.loading));
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if (lipstickSwatchesAdapter != null) {
                if (lipstickSwatchesAdapter.getSelectedImages().size() > 0) {
                    for (GalleryImages images : lipstickSwatchesAdapter.getSelectedImages()) {
                        File file = new File(images.getImagePath());
                        if (file.exists()) {
                            if (file.delete()) {
                                lipstickSwatchesAdapter.removeFromMainList(images);
//                                Log.e("-->", "file Deleted");

                            } else {
//                                Log.e("-->", "file not Deleted");
                            }
                        } else {
//                            Log.e("-->", "file not exists");
                        }
                    }
                    MediaScannerConnection.scanFile(LipstickSwatchesActivity.this, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
//                            Log.e("ExternalStorage", "Scanned " + path + ":");
//                            Log.e("ExternalStorage", "-> uri=" + uri);
                        }
                    });
                    return true;
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dialog.dismiss();
            if (aBoolean) {
                hideDeleteButton();
                AppUtil.showToast(LipstickSwatchesActivity.this, "Images deleted");
                lipstickSwatchesAdapter.notifyDataSetChanged();
            } else {
                AppUtil.showToast(LipstickSwatchesActivity.this, getResources().getString(R.string.something_wrong_with_delete));
            }

        }
    }

    private void loadAds() {
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build(); //.addTestDevice("CB120A73743E36FAECCAD807FDB46D29")
        mAdView.loadAd(adRequest);
    }
}
