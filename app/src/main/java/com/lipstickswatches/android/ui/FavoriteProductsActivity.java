package com.lipstickswatches.android.ui;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.adapter.ListsAdapter;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.ApplicationController;

import java.util.Collections;
import java.util.List;

public class FavoriteProductsActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView rvFavoriteList;
    ListsAdapter listsAdapter;
    List<Products> productList;

    ImageButton ibClose;
    TextView toolbar;
    AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_products);
        init();
    }

    private void init() {
        setupToolbar();
        rvFavoriteList = findViewById(R.id.rvFavoriteList);

        GridLayoutManager glm = new GridLayoutManager(this, 2);
        rvFavoriteList.setLayoutManager(glm);

        productList = ApplicationController.getDatabaseObj().productDAO().getFavoritedProducts();
        if (productList.size() > 0) {
            Collections.reverse(productList);
            listsAdapter = new ListsAdapter(productList, this, true, true);
            rvFavoriteList.setAdapter(listsAdapter);
        } else {
            AppUtil.showToast(this, getString(R.string.no_favorites));
        }
        loadAds();
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setText(getString(R.string.favorited_products));

        ibClose = findViewById(R.id.ibClose);
        ibClose.setOnClickListener(this);

        AppUtil.changeToolbarFont(this, toolbar);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibClose:
                finish();
                break;
        }
    }
    private void loadAds() {
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build(); //.addTestDevice("CB120A73743E36FAECCAD807FDB46D29")
        mAdView.loadAd(adRequest);
    }
}
