package com.lipstickswatches.android.ui;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ForgotPasswordActivity extends BaseActivitiy implements View.OnClickListener {
    TextInputEditText etEmail;
    Button bReset;
    String email;

    ImageButton ibBack;
    TextView tvToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        init();
    }

    private void init() {
        tvToolbarTitle = findViewById(R.id.tvToolbarTitle);
        tvToolbarTitle.setText(getString(R.string.forgot_password));

        AppUtil.changeToolbarFont(this, tvToolbarTitle);

        ibBack = findViewById(R.id.ibBack);

        etEmail = findViewById(R.id.etEmail);
        bReset = findViewById(R.id.bReset);

        ibBack.setOnClickListener(this);
        bReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibBack:
                finish();
                break;

            case R.id.bReset:
                email = etEmail.getText().toString();
                if (formValidation()) {
                    showProgress();
                    Map<String, String> params = new HashMap<>();
                    params.put("email", email);
                    NetworkManager.getInstance().post("forget_password", params, new RequestResponseListener<Object>() {
                        @Override
                        public void onResult(Integer response, Object object) {
                            dismissProgress();
                            switch (response) {
                                case Constants.WebApi.Response.NO_INTERNET:
                                    AppUtil.showToast(ForgotPasswordActivity.this, getString(R.string.no_internet));
                                    break;
                                case Constants.WebApi.Response.NOT_FOUND:
                                    AppUtil.showToast(ForgotPasswordActivity.this, getString(R.string.email_not_found));
                                    break;
                                case Constants.WebApi.Response.SUCCESS:
                                    AppUtil.showToast(ForgotPasswordActivity.this, getString(R.string.recovery_password_sent));
                                    List<Integer> flags = new ArrayList<>();
                                    flags.add(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    flags.add(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    transitionActivitiesFlags(ForgotPasswordActivity.this, LoginActivity.class, true, flags);
                                    break;
                                case Constants.WebApi.Response.TIMEOUT:
                                    AppUtil.showToast(ForgotPasswordActivity.this, getString(R.string.no_internet));
                                    break;
                                case Constants.WebApi.Response.UNAUTHORIZED:
                                    AppUtil.showToast(ForgotPasswordActivity.this, getString(R.string.account_suspended));
                                    break;
                            }
                        }
                    });
                }
        }
    }

    private boolean formValidation() {
        if (email.equals("")) {
            etEmail.setError(getResources().getString(R.string.email_required));
        } else if (!AppUtil.isEmailValid(email)) {
            etEmail.setError(getResources().getString(R.string.wrong_email_format));
        } else {
            etEmail.setError(null);
            return true;
        }
        return false;
    }
}
