package com.lipstickswatches.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.fragments.CameraFragment;
import com.lipstickswatches.android.fragments.HomeFragment;
import com.lipstickswatches.android.fragments.ListFragment;
import com.lipstickswatches.android.utils.AppRater;
import com.lipstickswatches.android.utils.Constants;

public class MainContainerActivity extends BaseActivitiy implements View.OnClickListener {
    ImageView ivHome, ivLipstick, ivList;
    FragmentTransaction ft;
    boolean doubleBackToExitPressedOnce = false;
    private BroadcastReceiver mMessageReceiver;
    private ListFragment listFragment;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_container);
        init();
    }

    private void init() {
        AppRater.appLaunched(this);
        setupFooter();
        loadAds();
    }

    private void setupFooter() {

        ivList = findViewById(R.id.ivList);
        ivLipstick = findViewById(R.id.ivLipstick);
        ivHome = findViewById(R.id.ivHome);
        ivList.setOnClickListener(this);
        ivLipstick.setOnClickListener(this);
        ivHome.setOnClickListener(this);
        Products product = getIntent().getParcelableExtra("product");
        boolean disableOtherButtons = getIntent().getBooleanExtra("disable_other_buttons", false);
        if (product != null) {
            startLipstickFragment(product);
            if (disableOtherButtons) {
                ivList.setEnabled(false);
                ivHome.setEnabled(false);
            }
        } else {
            setupDefaultView();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case Constants.AppIntents.ACTION_OPEN_LIPSTICKFRAGMENT:
                        startLipstickFragment((Products) intent.getParcelableExtra("product"));
                        break;
                    case Constants.AppIntents.ACTION_REFRESH_MAIN_LISTS:
                        if (listFragment != null) {
                            Products product = intent.getParcelableExtra("product");
                            listFragment.updateFragmentLists(Constants.MAIN_LIST, product);
                        }
                        break;
                    case Constants.AppIntents.ACTION_REFRESH_TRENDING_LISTS:
                        if (listFragment != null) {
                            Products product = intent.getParcelableExtra("product");
                            listFragment.updateFragmentLists(Constants.TRENDING_LIST, product);
                        }
                        break;
                }

            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.AppIntents.ACTION_OPEN_LIPSTICKFRAGMENT);
        filter.addAction(Constants.AppIntents.ACTION_REFRESH_MAIN_LISTS);
        filter.addAction(Constants.AppIntents.ACTION_REFRESH_TRENDING_LISTS);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, filter);

    }

    @Override
    public void onBackPressed() {
        if (getIntent().getParcelableExtra("product") != null
                && getIntent().getBooleanExtra("disable_other_buttons", false)) {
            finish();
            return;
        }
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            showToast(getString(R.string.press_back_again));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            getSupportFragmentManager().popBackStack();
            removeCurrentFragment();
        }
    }

    public void removeCurrentFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment currentFrag = getSupportFragmentManager().findFragmentById(R.id.flContainer);

        if (currentFrag != null)
            transaction.remove(currentFrag);

        transaction.commit();
    }

    private void startLipstickFragment(Products product) {
        changeFooterIcon(2);
        ft = getSupportFragmentManager().beginTransaction();
        CameraFragment fragment = new CameraFragment();
        Bundle b = new Bundle();
        b.putParcelable("product", product);
        fragment.setArguments(b);
        ft.replace(R.id.flContainer, fragment);
        ft.commit();
    }

    private void setupDefaultView() {
        changeFooterIcon(1);
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.flContainer, new HomeFragment());
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivHome:
                changeFooterIcon(1);
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.flContainer, new HomeFragment());
                ft.commit();
                break;
            case R.id.ivLipstick:
                changeFooterIcon(2);
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.flContainer, new CameraFragment());
                ft.commit();
                break;
            case R.id.ivList:
                changeFooterIcon(3);
                ft = getSupportFragmentManager().beginTransaction();
                listFragment = new ListFragment();
                ft.replace(R.id.flContainer, listFragment);
                ft.commit();
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
//                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }

                break;

        }
    }

    private void changeFooterIcon(int i) {
        switch (i) {
            case 1:
                ivHome.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
                ivLipstick.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                ivList.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                break;
            case 2:
                ivHome.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                ivLipstick.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
                ivList.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
                break;
            case 3:
                ivHome.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                ivLipstick.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                ivList.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    private void loadAds() {

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.intersial_ad_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build()); //.addTestDevice("CB120A73743E36FAECCAD807FDB46D29")
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });
    }
}
