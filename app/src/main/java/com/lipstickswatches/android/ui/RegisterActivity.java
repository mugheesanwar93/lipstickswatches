package com.lipstickswatches.android.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.pojo.User;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.Connectivity;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.SharedPrefUtils;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterActivity extends BaseActivitiy implements View.OnClickListener {
    private static final int PERMISSION_FLAG = 1;
    private static final String REQUIRED_PERMISSIONS[] = {
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    TextView tvLogin;
    CircleImageView civProfilePicture;
    TextInputEditText etName, etEmail, etPassword, etConfirmPassword;
    Button bRegister;
    String email, password, confirmPassword, fullname;
    //Toolbar
    TextView tvToolbarTitle;
    ImageButton ibBack;
    Bitmap profilePictureBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
    }

    private void init() {
        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);

        tvLogin = findViewById(R.id.tvLogin);
        tvToolbarTitle = findViewById(R.id.tvToolbarTitle);
        tvToolbarTitle.setText(getString(R.string.register));

        AppUtil.changeToolbarFont(this, tvToolbarTitle);

        bRegister = findViewById(R.id.bRegister);
        ibBack = findViewById(R.id.ibBack);

        civProfilePicture = findViewById(R.id.civProfilePicture);

        tvLogin.setOnClickListener(this);
        civProfilePicture.setOnClickListener(this);
        bRegister.setOnClickListener(this);
        ibBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvLogin:
                finish();
                break;

            case R.id.civProfilePicture:
                if (Build.VERSION.SDK_INT >= 23) {
                    requiredPermission();
                } else {
                    if (Connectivity.isConnected(getApplicationContext())) {
                        CropImage.startPickImageActivity(this);
                    } else {
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_internet));
                    }
                }
                break;

            case R.id.bRegister:
                email = etEmail.getText().toString();
                password = etPassword.getText().toString();
                confirmPassword = etConfirmPassword.getText().toString();
                fullname = etName.getText().toString();
                if (formValidation()) {
                    if (Connectivity.isConnected(getApplicationContext())) {
                        showProgress();
                        registerService();
//                        registerService.MainService(email, password, fullname);
                    } else {
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_internet));
                    }
                }
                break;

            case R.id.ibBack:
                finish();
                break;
        }
    }

    private boolean formValidation() {
        if (email.equals("")) {
            etEmail.setError(getResources().getString(R.string.email_required));
        } else if (password.equals("")) {
            etPassword.setError(getResources().getString(R.string.password_required));
        } else if (fullname.equals("")) {
            etName.setError(getResources().getString(R.string.fullname_required));
        } else if (confirmPassword.equals("")) {
            etConfirmPassword.setError(getResources().getString(R.string.password_required));
        } else if (!password.equals(confirmPassword)) {
            etConfirmPassword.setError(getResources().getString(R.string.password_do_not_match));
        } else if (!AppUtil.isEmailValid(email)) {
            etEmail.setError(getResources().getString(R.string.wrong_email_format));
        } else {
            etEmail.setError(null);
            etPassword.setError(null);
            etName.setError(null);
            etConfirmPassword.setError(null);
            return true;
        }
        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            startCropImageActivity(imageUri);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                try {
                    profilePictureBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                civProfilePicture.setImageBitmap(profilePictureBitmap);
                saveBitmap(profilePictureBitmap);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri).setAspectRatio(1, 1)
                .start(this);
    }

    private void requiredPermission() {
        ArrayList<String> permission = new ArrayList<>();
        for (String per : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, per) != PackageManager.PERMISSION_GRANTED) {
                permission.add(per);
            }
        }
        if (permission.size() > 0) {
            ActivityCompat.requestPermissions(this, permission.toArray(new String[permission.size()]), PERMISSION_FLAG);
        } else {
            CropImage.startPickImageActivity(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_FLAG) {
            Map<String, Integer> perms = new HashMap<>();
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    perms.put(permissions[i], grantResults[i]);
                }
                if (perms.get(REQUIRED_PERMISSIONS[0]).equals(PackageManager.PERMISSION_GRANTED)
                        && perms.get(REQUIRED_PERMISSIONS[1]).equals(PackageManager.PERMISSION_GRANTED)) {
                    CropImage.startPickImageActivity(this);
                } else {
                    AppUtil.showToast(this, getResources().getString(R.string.need_all_permissions));
                }
            }
        }
    }

    private void registerService() {
        showProgress();
        Map<String, String> params = new HashMap<>();
        params.put("name", fullname);
        params.put("email", email);
        params.put("password", password);
        NetworkManager.getInstance().post("user_register", params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                dismissProgress();
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        AppUtil.showToast(RegisterActivity.this, getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        AppUtil.showToast(RegisterActivity.this, getString(R.string.username_not_found));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        Gson gson = new Gson();
                        User[] user = gson.fromJson((String) object, User[].class);
//                        Log.e("USER", user[0].getName());
                        AppUtil.saveUserSession(user[0]);
//
                        List<Integer> flags = new ArrayList<>();
                        flags.add(Intent.FLAG_ACTIVITY_NEW_TASK);
                        flags.add(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        if (SharedPrefUtils.readBoolean("is_first_user", true)) {
//                            transitionActivitiesFlags(RegisterActivity.this, TutorialActivity.class, true, flags);
                            Intent intent = new Intent(RegisterActivity.this, TutorialActivity.class);
                            intent.putExtra("is_from_settings", false);
                            startActivity(intent);
                            SharedPrefUtils.writeBoolean("is_first_user", false);
                        } else {
                            transitionActivitiesFlags(RegisterActivity.this, MainContainerActivity.class, true, flags);
                        }
                        break;
                    case Constants.WebApi.Response.TIMEOUT:
                        AppUtil.showToast(RegisterActivity.this, getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.UNAUTHORIZED:
                        AppUtil.showToast(RegisterActivity.this, getString(R.string.account_suspended));
                        break;

                    case Constants.WebApi.Response.USERNAME_ALREADY_EXISTS:
                        AppUtil.showToast(RegisterActivity.this, getString(R.string.user_exists));
                        break;
                }
            }
        });
    }


//    @Override
//    public void update(Observable o, Object arg) {
//        if (o == registerService) {
//            dismissProgress();
//            if (registerService.getResponse().getResponseData().getSuccess()) {
//                AppUtil.saveUserSession(registerService.getResponse().getResponseData().getData().get(0));
//                List<Integer> flags = new ArrayList<>();
//                flags.add(Intent.FLAG_ACTIVITY_NEW_TASK);
//                flags.add(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                transitionActivitiesFlags(this, LoginActivity.class, true, flags);
//            } else {
//                AppUtil.showToast(this, registerService.getResponse().getResponseData().getMessage());
//            }
//        }
//    }
}
