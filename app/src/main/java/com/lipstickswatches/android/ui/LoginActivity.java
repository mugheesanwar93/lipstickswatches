package com.lipstickswatches.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.pojo.User;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.Connectivity;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.SharedPrefUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LoginActivity extends BaseActivitiy implements View.OnClickListener {
    TextInputEditText etEmail, etPassword;
    Button bLogin, bFb;
    LoginButton bFacebookLogin;
    TextView tvRegister, tvForgotPassword;

    String email, password, firstName = "", imageUrl;
    private CallbackManager callbackManager;

    Integer loginState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(getApplication());
        callbackManager = CallbackManager.Factory.create();
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }
        setContentView(R.layout.activity_login);
        init();
    }

    private void init() {
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);

        tvRegister = findViewById(R.id.tvRegister);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);

        bLogin = findViewById(R.id.bLogin);
        bFb = findViewById(R.id.bFb);

        bFacebookLogin = findViewById(R.id.bFacebookLogin);

        bLogin.setOnClickListener(this);
        bFb.setOnClickListener(this);
        bFacebookLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bLogin:
                hideKeyBoard(findViewById(android.R.id.content));
                email = etEmail.getText().toString();
                password = etPassword.getText().toString();
                if (formValidation()) {
                    loginState = 1;
                    loginService("user_login");
                }
                break;

            case R.id.bFacebookLogin:
                hideKeyBoard(findViewById(android.R.id.content));
                if (Connectivity.isConnected(getApplicationContext())) {
                    showProgress();
                    facebookSignIn();
                } else {
                    showToast(getString(R.string.no_internet));
                }
                break;

            case R.id.tvRegister:
                startActivity(new Intent(this, RegisterActivity.class));
                break;

            case R.id.tvForgotPassword:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                break;

            case R.id.bFb:
                bFacebookLogin.performClick();
                break;
        }
    }

    private void facebookSignIn() {
        bFacebookLogin.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_friends"));
        bFacebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                GraphRequest request = GraphRequest.newMeRequest(
                        accessToken,
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
//                                Bundle bFacebookData = getFacebookData(object);
                                try {
                                    email = object.getString("email");
                                    password = object.getString("id").substring(0, 6);
                                    firstName = object.getString("first_name");
                                    imageUrl = object.getString("image_url");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if (email == null) {
                                    try {
                                        email = Profile.getCurrentProfile().getId() + "@facebook.com";
                                    } catch (Exception e) {
//                                        Log.d("Facebook login problem", "Couldn't getId()");
                                    }
                                }
//                                password = Profile.getCurrentProfile().getId().substring(0, 6);
//                                firstName = Profile.getCurrentProfile().getFirstName();
//                                imageUrl = String.valueOf(Profile.getCurrentProfile().getProfilePictureUri(150, 150));
                                loginState = 2;
                                loginService("user_register");
                            }
                        });
//edit this to fix extra code
                Bundle parameters = new Bundle();
                parameters.putString("fields", "email, first_name, last_name");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                dismissProgress();
            }

            @Override
            public void onError(FacebookException exception) {
                dismissProgress();
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    private boolean formValidation() {
        if (email.equals("")) {
            etEmail.setError(getResources().getString(R.string.email_required));
        } else if (password.equals("")) {
            etPassword.setError(getResources().getString(R.string.password_required));
        } else if (!AppUtil.isEmailValid(email)) {
            etEmail.setError(getResources().getString(R.string.wrong_email_format));
        } else {
            etEmail.setError(null);
            etPassword.setError(null);
            return true;
        }
        return false;
    }

    private void loginService(String path) {
        showProgress();
        Map<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);
        params.put("name", firstName);
        NetworkManager.getInstance().post(path, params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                dismissProgress();
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        showToast(getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        showToast(getString(R.string.username_not_found));
                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut();
                        }
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        Gson gson = new Gson();
                        User[] user = gson.fromJson((String) object, User[].class);
//                        Log.e("USER", user[0].getName());
                        AppUtil.saveUserSession(user[0]);
                        List<Integer> flags = new ArrayList<>();
                        flags.add(Intent.FLAG_ACTIVITY_NEW_TASK);
                        flags.add(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        if (SharedPrefUtils.readBoolean("is_first_user", true)) {
//                            transitionActivitiesFlags(LoginActivity.this, TutorialActivity.class, true, flags);
                            Intent intent = new Intent(LoginActivity.this, TutorialActivity.class);
                            intent.putExtra("is_from_settings", false);
                            startActivity(intent);
                            SharedPrefUtils.writeBoolean("is_first_user", false);
                        } else {
                            transitionActivitiesFlags(LoginActivity.this, MainContainerActivity.class, true, flags);
                        }
                        break;
                    case Constants.WebApi.Response.TIMEOUT:
                        showToast(getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.UNAUTHORIZED:
                        showToast(getString(R.string.account_suspended));
                        break;
                }
            }
        });
    }

}
