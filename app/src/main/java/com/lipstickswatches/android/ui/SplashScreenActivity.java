package com.lipstickswatches.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.pojo.ProductResponse.Brand;
import com.lipstickswatches.android.pojo.ProductResponse.Datum;
import com.lipstickswatches.android.pojo.ProductResponse.Type;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.Connectivity;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SplashScreenActivity extends BaseActivitiy {
    private List<Integer> flags = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        init();
    }

    private void init() {
        flags.add(Intent.FLAG_ACTIVITY_NEW_TASK);
        flags.add(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if (SharedPrefUtils.readInteger("loginState", 0) > 0) {
            if (Connectivity.isConnected(getApplicationContext())) {

                AppUtil.removeLocalData();
                getAllProducts();

            } else {
                transitionActivitiesFlags(SplashScreenActivity.this, MainContainerActivity.class, true, flags);
            }
        } else {
            transitionActivitiesFlags(SplashScreenActivity.this, LoginActivity.class, true, flags);
        }
    }

    private void getAllProducts() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));

        NetworkManager.getInstance().post("get_all_products", params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        Gson gson = new Gson();
                        ArrayList<Datum> productList = gson.fromJson((String) object, new TypeToken<ArrayList<Datum>>() {
                        }.getType());
                        AppUtil.removeProducts();
                        AppUtil.saveProducts(productList);
                        getAllBrands();
                        break;
                }
            }
        });
    }

    private void getAllBrands() {
        NetworkManager.getInstance().get("get_all_brands", new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        dismissProgress();
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        dismissProgress();
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        Gson gson = new Gson();
                        ArrayList<Brand> brandList = gson.fromJson((String) object, new TypeToken<ArrayList<Brand>>() {
                        }.getType());
                        brandList.add(new Brand(
                                -1, "All", "1"
                        ));
                        AppUtil.removeBrands();
                        AppUtil.saveBrands(brandList);
                        getAllTypes();
                        break;
                }
            }
        });
    }

    private void getAllTypes() {
        NetworkManager.getInstance().get("get_all_types", new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        dismissProgress();
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        dismissProgress();
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        Gson gson = new Gson();
                        ArrayList<Type> typeList = gson.fromJson((String) object, new TypeToken<ArrayList<Type>>() {
                        }.getType());
                        typeList.add(new Type(
                                -1, "All", "1"
                        ));
                        AppUtil.removeTypes();
                        AppUtil.saveTypes(typeList);
                        transitionActivitiesFlags(SplashScreenActivity.this, MainContainerActivity.class, true, flags);

                        break;
                }
            }
        });
    }
}
