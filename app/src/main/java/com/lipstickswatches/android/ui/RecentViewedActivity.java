package com.lipstickswatches.android.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.adapter.ListsAdapter;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.ApplicationController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecentViewedActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView rvRecentViewedList;
    private ListsAdapter listsAdapter;
    private List<Products> productList;
    private SwipeRefreshLayout srlNewlyAddedList;
    private ImageButton ibClose, ibDelete;
    private TextView toolbar;
    private AdView mAdView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_products);
        init();
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setText(getString(R.string.recent_viewed));

        ibClose = findViewById(R.id.ibClose);
        ibClose.setOnClickListener(this);
        ibDelete = findViewById(R.id.ibDelete);
        ibDelete.setVisibility(View.VISIBLE);
        ibDelete.setOnClickListener(this);
        AppUtil.changeToolbarFont(this, toolbar);
    }

    private void init() {
        setupToolbar();
        rvRecentViewedList = findViewById(R.id.rvFavoriteList);
        srlNewlyAddedList = findViewById(R.id.srlNewlyAddedList);
        srlNewlyAddedList.setEnabled(false);
        GridLayoutManager glm = new GridLayoutManager(this, 2);
        rvRecentViewedList.setLayoutManager(glm);

        productList = ApplicationController.getDatabaseObj().recentViewedDAO().getRecentViewedProducts();
        if (productList.size() > 0) {
            Collections.reverse(productList);
            listsAdapter = new ListsAdapter(productList, this, false, false, true);
            rvRecentViewedList.setAdapter(listsAdapter);
        } else {
            AppUtil.showToast(this, getString(R.string.no_recent_viewed));
        }
        loadAds();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibClose:
                finish();
                break;
            case R.id.ibDelete:
                AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.CustomDialogTheme);
                dialog.setTitle("Clear History");
                dialog.setMessage("Clear Your Recent Viewed History?");
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ApplicationController.getDatabaseObj().recentViewedDAO().deleteRecentViewed();
                        listsAdapter = new ListsAdapter(new ArrayList<>(), RecentViewedActivity.this, false, false, true);
                        rvRecentViewedList.setAdapter(listsAdapter);
                        AppUtil.showToast(RecentViewedActivity.this, "History Cleared!");
                    }
                });
                dialog.setNegativeButton("No", null);
                dialog.show();
                break;
        }
    }

    private void loadAds() {
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build(); //.addTestDevice("CB120A73743E36FAECCAD807FDB46D29")
        mAdView.loadAd(adRequest);
    }

}
