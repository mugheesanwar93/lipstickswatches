package com.lipstickswatches.android.ui;

import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.adapter.ListsAdapter;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.pojo.ProductResponse.Datum;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.ApplicationController;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchActivity extends BaseActivitiy implements SearchView.OnQueryTextListener {
    Toolbar toolbar;
    RecyclerView rvList;
    ListsAdapter listsAdapter;
    List<Products> productList;
    SearchView svList;
    AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        init();
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.md_nav_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        svList = findViewById(R.id.svList);
        svList.setOnQueryTextListener(this);

        rvList = findViewById(R.id.rvList);
        GridLayoutManager llm = new GridLayoutManager(this, 2);
        rvList.setLayoutManager(llm);

        if (ApplicationController.getDatabaseObj().productDAO().getAllProducts().size() < 0) {
            callService(true);
        } else {
            productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts();
            listsAdapter = new ListsAdapter(productList, this, true);
            rvList.setAdapter(listsAdapter);

        }
        loadAds();
    }

    public void callService(boolean isProgress) {

        if (isProgress) {
            showProgress();
        }
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));

        NetworkManager.getInstance().post("get_all_products", params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                dismissProgress();
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        Gson gson = new Gson();
                        ArrayList<Datum> newProductList = gson.fromJson((String) object, new TypeToken<ArrayList<Datum>>() {
                        }.getType());
                        AppUtil.removeProducts();
                        AppUtil.saveProducts(newProductList);
                        productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts();
                        listsAdapter = new ListsAdapter(productList, SearchActivity.this, true);
                        rvList.setAdapter(listsAdapter);
                        break;
                }
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listsAdapter.getFilter().filter(newText);
        return true;
    }

    private void loadAds() {
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build(); //.addTestDevice("CB120A73743E36FAECCAD807FDB46D29")
        mAdView.loadAd(adRequest);
    }

}
