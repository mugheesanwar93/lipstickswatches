package com.lipstickswatches.android.ui;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.adapter.ListsAdapter;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.pojo.ProductResponse.Datum;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.ApplicationController;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewlyAddedActivity extends BaseActivitiy implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView rvNewlyAddedList;
    private ListsAdapter listsAdapter;
    private SwipeRefreshLayout srlNewlyAddedList;
    private List<Products> productList;
    private ImageButton ibClose;
    private TextView toolbar;
    private AdView mAdView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_products);
        init();
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setText(getString(R.string.newly_added));
        ibClose = findViewById(R.id.ibClose);
        ibClose.setOnClickListener(this);
        AppUtil.changeToolbarFont(this, toolbar);
    }

    private void init() {
        setupToolbar();
        rvNewlyAddedList = findViewById(R.id.rvFavoriteList);
        GridLayoutManager glm = new GridLayoutManager(this, 2);
        rvNewlyAddedList.setLayoutManager(glm);
        srlNewlyAddedList = findViewById(R.id.srlNewlyAddedList);
        srlNewlyAddedList.setOnRefreshListener(this);
        if (ApplicationController.getDatabaseObj().productDAO().getAllProducts().size() <= 0) {
            callService(true);
        } else {
            productList = ApplicationController.getDatabaseObj().productDAO().getNewlyAddedProducts();
            listsAdapter = new ListsAdapter(productList, this, false, true);
            rvNewlyAddedList.setAdapter(listsAdapter);
        }
        loadAds();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibClose:
                finish();
                break;
        }
    }

    private void callService(boolean isProgress) { //need to change service to trending products
        if (isProgress) {
            showProgress();
        }
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));
        NetworkManager.getInstance().post("get_all_products", params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                if (srlNewlyAddedList.isRefreshing()) {
                    srlNewlyAddedList.setRefreshing(false);
                }
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        dismissProgress();
                        AppUtil.showToast(NewlyAddedActivity.this, getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        dismissProgress();
                        AppUtil.showToast(NewlyAddedActivity.this, getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        dismissProgress();
                        Gson gson = new Gson();
                        ArrayList<Datum> newProductList = gson.fromJson((String) object, new TypeToken<ArrayList<Datum>>() {
                        }.getType());

                        AppUtil.removeProducts();
                        AppUtil.saveProducts(newProductList);

                        productList = ApplicationController.getDatabaseObj().productDAO().getNewlyAddedProducts();
                        listsAdapter = new ListsAdapter(productList, NewlyAddedActivity.this, false, true);
                        rvNewlyAddedList.setAdapter(listsAdapter);
                        break;
                }
            }
        });


    }

    @Override
    public void onRefresh() {
        callService(false);
    }

    private void loadAds() {
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build(); //.addTestDevice("CB120A73743E36FAECCAD807FDB46D29")
        mAdView.loadAd(adRequest);
    }
}
