package com.lipstickswatches.android.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.pojo.ProductResponse.Datum;
import com.lipstickswatches.android.pojo.ProductResponse.Product;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.ApplicationController;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.CustomImageView;
import com.lipstickswatches.android.utils.SharedPrefUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LipstickDetailsActivity extends BaseActivitiy implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    Button bTryItOn;
    ImageView ivColor;
    CustomImageView ivImage;
    ImageButton ibCart, ibCross, ibShare, ibDelete;
    CheckBox cbFavourite;
    TextView tvName, tvBrand, tvColorName, tvType, tvDiscount, tvDetail;
    LinearLayout llDiscount;
    //    Datum product;
    String productId;
    Products product;
    View view;
    Uri imageUri;
    LinearLayout llDetails;
    Typeface typeface;

    BottomSheetBehavior sheetBehavior;


    ImageButton ibClose;
    TextView toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lipstick_details);
        init();
    }

    private void init() {
        setupToolbar();

        productId = getIntent().getExtras().getString("productId", "0");
        imageUri = Uri.parse(getIntent().getExtras().getString("imageUri"));
//changes here for type
        llDetails = findViewById(R.id.llDetails);
        llDetails.setOnClickListener(this);
        view = findViewById(R.id.view);
        ivImage = findViewById(R.id.ivImage);
        ivColor = findViewById(R.id.ivColor);
        tvName = findViewById(R.id.tvName);
        tvBrand = findViewById(R.id.tvBrand);
        tvColorName = findViewById(R.id.tvColorName);
        tvType = findViewById(R.id.tvType);
        tvDiscount = findViewById(R.id.tvDiscount);
        tvDetail = findViewById(R.id.tvDetail);
        llDiscount = findViewById(R.id.llDiscount);

        ibCart = findViewById(R.id.ibCart);
        ibCross = findViewById(R.id.ibCross);

        cbFavourite = findViewById(R.id.cbFavourite);
        bTryItOn = findViewById(R.id.bTryItOn);

        if (productId != null && !productId.equals("0")) {
            product = ApplicationController.getDatabaseObj().productDAO().getProductById(productId);
            if (product != null) {
                setupValues();
            } else {
                callService();
            }
        }
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setText(getString(R.string.lipstick_details));

        ibClose = findViewById(R.id.ibClose);
        ibClose.setOnClickListener(this);
        ibShare = findViewById(R.id.ibShare);
        ibShare.setVisibility(View.VISIBLE);
        ibShare.setOnClickListener(this);
        ibDelete = findViewById(R.id.ibDelete);
        ibDelete.setVisibility(View.VISIBLE);
        ibDelete.setOnClickListener(this);
        AppUtil.changeToolbarFont(this, toolbar);
    }

    private void setupValues() {
        ivImage.setImageURI(imageUri);

        ivColor.setColorFilter(Color.parseColor(product.getColorCode()));
        tvName.setText(product.getName());
        tvBrand.setText(product.getBrandName());
        tvBrand.setTypeface(typeface);
        tvColorName.setText(product.getColorName());
        tvColorName.setTextColor(Color.parseColor(product.getColorCode()));

        tvType.setText(product.getBrandName());
        tvDiscount.setText(product.getIsDiscount());

        ibCart.setOnClickListener(this);
        bTryItOn.setOnClickListener(this);


        if (product.getIsFavoriteByUser().equals(1)) {
            cbFavourite.setChecked(true);
        }

        if (product.getIsDiscount().equals("0")) {
            llDiscount.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
        } else {
            llDiscount.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
            tvDiscount.setText(product.getDiscountCode());
        }

        sheetBehavior = BottomSheetBehavior.from(llDetails);

        cbFavourite.setOnCheckedChangeListener(this);
    }

    private void callService() {
        showProgress();
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));
        params.put("product_id", productId);

        NetworkManager.getInstance().post("get_all_products", params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                dismissProgress();
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        dismissProgress();
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        dismissProgress();
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS://test this part
                        Gson gson = new Gson();
                        ArrayList<Datum> productList = gson.fromJson((String) object, new TypeToken<ArrayList<Datum>>() {
                        }.getType());
                        if (productList.size() > 0) {
                            Product newProduct = productList.get(0).getProduct();
//                            product = productList.get(0);
                            product = new Products(
                                    newProduct.getId(),
                                    newProduct.getName(),
                                    newProduct.getColorName(),
                                    newProduct.getColorCode(),
                                    newProduct.getStatus().getId(),
                                    newProduct.getDownloadLink(),
                                    newProduct.getDesignerId(),
                                    newProduct.getBrandId(),
                                    newProduct.getTypeId(),
                                    newProduct.getIsDiscount(),
                                    newProduct.getDiscountCode(),
                                    newProduct.getFavoritedCount(),
                                    newProduct.getDesigner().getName(),
                                    newProduct.getBrand().getName(),
                                    newProduct.getType().getName(),
                                    newProduct.getIsFavoriteByUser(),
                                    newProduct.getCreatedAt(),
                                    newProduct.getUpdatedAt()
                            );
                            setupValues();
                        }
                        break;
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bTryItOn:
                Intent intent = new Intent(this, MainContainerActivity.class);
                intent.putExtra("product", product);
                intent.putExtra("disable_other_buttons", true);
                startActivity(intent);
                break;

            case R.id.ibCart:
                AppUtil.openLink(product.getDownloadLink(), this);
                break;

            case R.id.ibClose:
                finish();
                break;
            case R.id.ibShare:
                AppUtil.shareImage(imageUri.getPath(), this);
                break;
            case R.id.ibDelete:
                AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.MyDialogTheme);
                dialog.setTitle("Delete Picture");
                dialog.setMessage("Remove from Swatche Factory?");
                dialog.setNegativeButton("No", null);
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        File file = new File(imageUri.getPath());
                        if (file.exists()) {
                            if (file.delete()) {
//                                Log.e("-->", "file Deleted");
                                MediaScannerConnection.scanFile(LipstickDetailsActivity.this, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                                    /*
                                     *   (non-Javadoc)
                                     * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                                     */
                                    public void onScanCompleted(String path, Uri uri) {
//                                        Log.e("ExternalStorage", "Scanned " + path + ":");
//                                        Log.e("ExternalStorage", "-> uri=" + uri);
                                    }
                                });
                                AppUtil.showToast(LipstickDetailsActivity.this, "Image deleted");
                                setResult(RESULT_OK);
                                finish();
                            } else {
//                                Log.e("-->", "file not Deleted");
                            }
                        } else {
//                            Log.e("-->", "file not exists");
                        }
                    }
                });
                dialog.setCancelable(true);
                dialog.show();
                break;
            case R.id.llDetails:
                if (sheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
        }
    }

    /**
     * Called when the checked state of a compound button has changed.
     *
     * @param buttonView The compound button view whose state has changed.
     * @param isChecked  The new checked state of buttonView.
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));
        params.put("product_id", String.valueOf(product.getId()));
        params.put("status", String.valueOf(isChecked ? 1 : 0));
        NetworkManager.getInstance().post("favorite_unfavorite_product", params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        AppUtil.showToast(getApplicationContext(), getString(R.string.username_not_found));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        JSONArray jb = null;
                        try {
                            jb = new JSONArray((String) object);
                            JSONObject jObj = jb.getJSONObject(0);
                            product.setIsFavoriteByUser(jObj.getInt("is_fav"));
                            ApplicationController.getDatabaseObj().productDAO().updateProductById(jObj.getInt("is_fav"), String.valueOf(product.getId()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case Constants.WebApi.Response.TIMEOUT:
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.UNAUTHORIZED:
                        AppUtil.showToast(getApplicationContext(), getString(R.string.account_suspended));
                        break;
                }
            }
        });
    }
}
