package com.lipstickswatches.android.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.ContentLoadingProgressBar;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.SharedPrefUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileActivity extends BaseActivitiy implements View.OnClickListener {
    CircleImageView civDisplayPicture;
    ContentLoadingProgressBar clpbLoading;
    RelativeLayout rlMyProfile, rlChangePassword, rlWatchTutorial, rlLogout;
    //    , rlMyFavourites, rlFavoritedProducts,
    TextView toolbar, tvName;
    //tvVersion,
    String image;
    int loginState;
    ImageView ibClose;

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View v = inflater.inflate(R.layout.activity_profile, container, false);
//        return init(v);
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
    }

    private void init() {
        loginState = SharedPrefUtils.readInteger("loginState", -1);
        setupToolbar();
        tvName = findViewById(R.id.tvName);
        tvName.setText(SharedPrefUtils.readString("name", ""));
        civDisplayPicture = findViewById(R.id.civDisplayPicture);
        civDisplayPicture.setOnClickListener(this);
        clpbLoading = findViewById(R.id.clpbLoading);
        rlMyProfile = findViewById(R.id.rlMyProfile);
//        rlMyFavourites = v.findViewById(R.id.rlMyFavourites);
//        rlFavoritedProducts = v.findViewById(R.id.rlFavoritedProducts);
        rlChangePassword = findViewById(R.id.rlChangePassword);
        rlWatchTutorial = findViewById(R.id.rlTutorial);
        rlLogout = findViewById(R.id.rlLogout);
        rlMyProfile.setOnClickListener(this);
//        rlMyFavourites.setOnClickListener(this);
//        rlFavoritedProducts.setOnClickListener(this);
        rlChangePassword.setOnClickListener(this);
        rlWatchTutorial.setOnClickListener(this);
        rlLogout.setOnClickListener(this);
        image = SharedPrefUtils.readString("image", "");
        if (!image.equals("") && image != null) {
            Picasso.get().load(Constants.imageUrl + image).into(civDisplayPicture, new Callback() {
                @Override
                public void onSuccess() {
                    clpbLoading.hide();
                }

                @Override
                public void onError(Exception e) {

                }
            });
        } else {
            clpbLoading.hide();
        }

    }

    private void setupToolbar() {

        toolbar = findViewById(R.id.toolbar);
        toolbar.setText(getString(R.string.my_account));
        AppUtil.changeToolbarFont(this, toolbar);
        ibClose = findViewById(R.id.ibClose);
        ibClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlMyProfile:
                transitionActivities(this, MyProfileActivity.class, false);
                break;

//            case R.id.rlMyFavourites:
//                transitionActivities(getActivity(), LipstickSwatchesActivity.class, false);
//                break;

            case R.id.rlChangePassword:
                transitionActivities(this, ChangePasswordActivity.class, false);
                break;
            case R.id.rlTutorial:
//                transitionActivities(this, TutorialActivity.class, false);
                Intent intent = new Intent(this, TutorialActivity.class);
                intent.putExtra("is_from_settings", true);
                startActivity(intent);
                break;
            case R.id.rlLogout:
                logoutConfirmationDialog();
                break;
            case R.id.ibClose:
                finish();
                break;

//            case R.id.rlFavoritedProducts:
//                transitionActivities(getActivity(), FavoriteProductsActivity.class, false);
//                break;
        }
    }

    public void logoutConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle(getString(R.string.logout_confirmation));
        builder.setMessage(getString(R.string.press_ok_to_logout));
        builder.setCancelable(false);

        String positiveText = this.getString(R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LogInScreen();
                    }
                });

        String negativeText = getString(R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }

    private void LogInScreen() {

        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }

        SharedPrefUtils.removeAll();

        List<Integer> flags = new ArrayList<>();
        flags.add(Intent.FLAG_ACTIVITY_NEW_TASK);
        flags.add(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        transitionActivitiesFlags(this, LoginActivity.class, true, flags);
    }

}
