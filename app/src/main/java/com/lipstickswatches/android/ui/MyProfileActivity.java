package com.lipstickswatches.android.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ContentLoadingProgressBar;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.pojo.User;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.Connectivity;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.SharedPrefUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfileActivity extends BaseActivitiy implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    TextInputEditText etEmail, etFullName, etDob, etGender;
    Button bSave;
    ImageButton ibClose;
    TextView toolbar;

    String email, fullName, dob, image;
    Integer genderId = 0;

    CircleImageView civDisplayPicture;
    ContentLoadingProgressBar clpbLoading;

    Bitmap profilePictureBitmap;
    String profilePictureMap = "";
    private static final int PERMISSION_FLAG = 1;
    private static final String REQUIRED_PERMISSIONS[] = {
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        init();
    }

    private void init() {
        setupToolbar();
        etEmail = findViewById(R.id.etEmail);
        etFullName = findViewById(R.id.etFullName);
        etDob = findViewById(R.id.etDob);
        etGender = findViewById(R.id.etGender);
        bSave = findViewById(R.id.bSave);
        bSave.setOnClickListener(this);

        ibClose = findViewById(R.id.ibClose);
        ibClose.setOnClickListener(this);

        etDob.setFocusable(false);
        etDob.setOnClickListener(this);

        etGender.setFocusable(false);
        etGender.setOnClickListener(this);

        civDisplayPicture = findViewById(R.id.civDisplayPicture);
        clpbLoading = findViewById(R.id.clpbLoading);

        civDisplayPicture.setOnClickListener(this);

        setupValues();
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setText(getString(R.string.edit_profile));

        AppUtil.changeToolbarFont(this, toolbar);
    }

    private void setupValues() {
        email = SharedPrefUtils.readString("userEmail", "");
        fullName = SharedPrefUtils.readString("name", "");
        dob = SharedPrefUtils.readString("birthday", "");
        genderId = Integer.valueOf(SharedPrefUtils.readString("gender", ""));
        image = SharedPrefUtils.readString("image", "");

        etEmail.setText(email);
        etFullName.setText(fullName);
        etDob.setText(dob);


        switch (genderId) {
            case 0:
                etGender.setText(getResources().getString(R.string.gender_unspecified));
                break;
            case 1:
                etGender.setText(getResources().getString(R.string.male));
                break;
            case 2:
                etGender.setText(getResources().getString(R.string.female));
                break;
        }

        if (!dob.equals("")) {
            String[] separated = dob.split(" ");
            dob = separated[0];
            if (!dob.equals("1.1.1900")) {
                etDob.setText(dob);
            }
            String[] separateBday = dob.split("\\.");
            dob = separateBday[2] + "." + separateBday[1] + "." + separateBday[0];
        }


        image = SharedPrefUtils.readString("image", "");

        if (!image.equals("") && image != null) {
            Picasso.get().load(Constants.imageUrl + image).into(civDisplayPicture, new Callback() {
                @Override
                public void onSuccess() {
                    clpbLoading.hide();
                }

                @Override
                public void onError(Exception e) {

                }
            });
        } else {
            clpbLoading.hide();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.civDisplayPicture:
                if (Build.VERSION.SDK_INT >= 23) {
                    requiredPermission();
                } else {
                    if (Connectivity.isConnected(getApplicationContext())) {
                        CropImage.startPickImageActivity(this);
                    } else {
                        AppUtil.showToast(getApplicationContext(), getString(R.string.no_internet));
                    }
                }
                break;

            case R.id.etDob:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "Datepickerdialog");
                break;

            case R.id.etGender:
                new MaterialDialog.Builder(this)
                        .title(R.string.select_gender)
                        .items(R.array.gender)
                        .itemsCallbackSingleChoice(genderId, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                etGender.setText(text);
                                genderId = which;
                                return true;
                            }
                        })
                        .positiveText(getString(R.string.ok))
                        .negativeText(getString(R.string.cancel))
                        .cancelable(false)
                        .show();
                break;

            case R.id.bSave:
                fullName = etFullName.getText().toString();
                if (!fullName.equals("")) {
                    showProgress();
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));
                    params.put("name", fullName);
                    params.put("birthday", dob);
                    params.put("gender", String.valueOf(genderId));
                    params.put("photo", profilePictureMap);
                    NetworkManager.getInstance().post("update_user_profile", params, new RequestResponseListener<Object>() {
                        @Override
                        public void onResult(Integer response, Object object) {
                            dismissProgress();
                            switch (response) {
                                case Constants.WebApi.Response.NO_INTERNET:
                                    showToast(getString(R.string.no_internet));
                                    break;
                                case Constants.WebApi.Response.NOT_FOUND:
                                    showToast(getString(R.string.profile_not_updated));
                                    break;
                                case Constants.WebApi.Response.SUCCESS:
                                    Gson gson = new Gson();
                                    User[] user = gson.fromJson((String) object, User[].class);
//                                    Log.e("USER", user[0].getName());
                                    AppUtil.saveUserSession(user[0]);
                                    showToast(getString(R.string.profile_updated));
                                    finish();
                                    break;
                                case Constants.WebApi.Response.TIMEOUT:
                                    showToast(getString(R.string.no_internet));
                                    break;
                                case Constants.WebApi.Response.UNAUTHORIZED:
                                    showToast(getString(R.string.account_suspended));
                                    break;
                            }
                        }
                    });
                } else {
                    etFullName.setError(getResources().getString(R.string.fullname_required));
                }
                break;

            case R.id.ibClose:
                finish();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            startCropImageActivity(imageUri);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                try {
                    profilePictureBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                civDisplayPicture.setImageBitmap(profilePictureBitmap);
//                saveBitmap(profilePictureBitmap);

                profilePictureMap = AppUtil.getStringImage(profilePictureBitmap);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri).setAspectRatio(1, 1)
                .start(this);
    }

    private void requiredPermission() {
        ArrayList<String> permission = new ArrayList<>();
        for (String per : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, per) != PackageManager.PERMISSION_GRANTED) {
                permission.add(per);
            }
        }
        if (permission.size() > 0) {
            ActivityCompat.requestPermissions(this, permission.toArray(new String[permission.size()]), PERMISSION_FLAG);
        } else {
            CropImage.startPickImageActivity(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_FLAG) {
            Map<String, Integer> perms = new HashMap<>();
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    perms.put(permissions[i], grantResults[i]);
                }
                if (perms.get(REQUIRED_PERMISSIONS[0]).equals(PackageManager.PERMISSION_GRANTED)
                        && perms.get(REQUIRED_PERMISSIONS[1]).equals(PackageManager.PERMISSION_GRANTED)) {
                    CropImage.startPickImageActivity(this);
                } else {
                    AppUtil.showToast(this, getResources().getString(R.string.need_all_permissions));
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        etDob.setText(dayOfMonth + "." + (monthOfYear + 1) + "." + year);
        dob = year + "." + (monthOfYear + 1) + "." + dayOfMonth;
    }
}
