package com.lipstickswatches.android.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.utils.SharedPrefUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mugheesanwar on 06/02/2018.
 */

public abstract class BaseActivitiy<T> extends AppCompatActivity {
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefUtils.init(getApplicationContext());
    }

    /**
     * This method is used to show ProgressDialog. It dismisses if there is any previous instance running and then initialize dialog again.
     */

    protected void showProgress() {
        dismissProgress();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    /**
     * This function dismisses if progress dialog is running and clear from memory.
     */

    protected void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    /**
     * This is a common method that shows the toast with the message sent to this method.
     *
     * @param msg Message to be shown in the toast.
     */

    protected void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * This function hides soft keyboard.
     *
     * @param view View passed to get window token to hide soft keyboard.
     */

    protected void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
        }
    }

    /**
     * This method reads value from Shared Preference and return userID.
     * @return The user ID from shared preferences. If value is not found it returns empty string.
     */

//    protected String getUserId() {
//        Gson gson = new Gson();
//        String json = getUserFromSharedPreference();
//        UserModel userModel = gson.fromJson(json, UserModel.class);
//        if (userModel == null) {
//            return "";
//        }
//        return userModel.getUserId();
//    }


    /**
     * This function is used to transition from one activity to another.
     *
     * @param current     Current activity from which we are transitioning
     * @param newActivity Activity class to which we are transitioning
     * @param isFinished  if we want to finish current Activity or not
     */
    protected void transitionActivities(Activity current, Class newActivity, boolean isFinished) {
        Intent intent = new Intent(current, newActivity);
        startActivity(intent);
        if (isFinished) {
            current.finish();
        }
    }


    /**
     * This function is used to transition from one activity to another.
     *
     * @param current     Current activity from which we are transitioning
     * @param newActivity Activity class to which we are transitioning
     * @param isFinished  if we want to finish current Activity or not
     * @param flags       list of Intent flags
     */
    protected void transitionActivitiesFlags(Activity current, Class newActivity, boolean isFinished, List<Integer> flags) {
        Intent intent = new Intent(current, newActivity);
        if (flags != null && flags.size() > 0) {
            for (Integer flag :
                    flags) {
                intent.addFlags(flag);
            }
        }
        startActivity(intent);
        if (isFinished) {
            current.finish();
        }
    }

    /**
     * This function returns default headers to be passed to Http client
     *
     * @return default headers
     */
    private Map<String, String> getDefaultHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json;");
        return headers;
    }


    @Override
    protected void onPause() {
        super.onPause();
        dismissProgress();
    }

    public static void showKeyBoard(View view) {
        if (view != null) {
            if (view instanceof EditText) {
                EditText editText = (EditText) view;
                editText.setSelection(editText.length());
            }
            InputMethodManager imm = (InputMethodManager)
                    view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static void hideKeyBoard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public File saveBitmap(Bitmap bitmap) {
        File file = null;
        if (bitmap != null) {
            File myDir = new File("/sdcard/lipstickSwatches/");
            myDir.mkdirs();
            file = new File(myDir, "profile_picture.jpg");
            try {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(file); //here is set your file path where you want to save or also here you can set file object directly
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream); // bitmap is your Bitmap instance, if you want to compress it you can compress reduce percentage
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.flush();
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file;
    }
}
