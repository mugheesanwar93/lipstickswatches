package com.lipstickswatches.android.ui;

import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.SharedPrefUtils;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends BaseActivitiy implements View.OnClickListener {
    ImageButton ibClose;
    TextView toolbar;
    TextInputEditText etOldPassword, etNewPassword, etConfirmNewPassword;
    Button bSave;

    String oldPassword, newPassword, confirmNewPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        init();
    }

    private void init() {
        setupToolbar();

        etOldPassword = findViewById(R.id.etOldPassword);
        etNewPassword = findViewById(R.id.etNewPassword);
        etConfirmNewPassword = findViewById(R.id.etConfirmNewPassword);

        ibClose = findViewById(R.id.ibClose);
        ibClose.setOnClickListener(this);

        bSave = findViewById(R.id.bSave);
        bSave.setOnClickListener(this);
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setText(getString(R.string.password_change));

        AppUtil.changeToolbarFont(this, toolbar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibClose:
                finish();
                break;

            case R.id.bSave:
                oldPassword = etOldPassword.getText().toString();
                newPassword = etNewPassword.getText().toString();
                confirmNewPassword = etConfirmNewPassword.getText().toString();
                if (formValidation()) {
                    showProgress();
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));
                    params.put("old_password", oldPassword);
                    params.put("new_password", newPassword);
                    NetworkManager.getInstance().post("change_password", params, new RequestResponseListener<Object>() {
                        @Override
                        public void onResult(Integer response, Object object) {
                            dismissProgress();
                            switch (response) {
                                case Constants.WebApi.Response.NO_INTERNET:
                                    showToast(getString(R.string.no_internet));
                                    break;
                                case Constants.WebApi.Response.NOT_FOUND:
                                    showToast(getString(R.string.username_not_found));
                                    break;
                                case Constants.WebApi.Response.SUCCESS:
                                    showToast(getString(R.string.password_change_successfully));
                                    finish();
                                    break;
                                case Constants.WebApi.Response.TIMEOUT:
                                    showToast(getString(R.string.no_internet));
                                    break;
                                case Constants.WebApi.Response.UNAUTHORIZED:
                                    showToast(getString(R.string.account_suspended));
                                    break;
                            }
                        }
                    });
                }
                break;
        }
    }

    private boolean formValidation() {
        if (oldPassword.equals("")) {
            etOldPassword.setError(getResources().getString(R.string.required_old_password));
        } else if (oldPassword.length() < 5) {
            etOldPassword.setError(getResources().getString(R.string.minimum_password_required));
        } else if (newPassword.equals("")) {
            etNewPassword.setError(getResources().getString(R.string.required_new_password));
        } else if (newPassword.length() < 5) {
            etNewPassword.setError(getResources().getString(R.string.minimum_password_required));
        } else if (confirmNewPassword.equals("")) {
            etConfirmNewPassword.setError(getResources().getString(R.string.required_confirm_new_password));
        } else if (!newPassword.equals(confirmNewPassword)) {
            etConfirmNewPassword.setError(getResources().getString(R.string.password_not_matching));
        } else {
            etOldPassword.setError(null);
            etNewPassword.setError(null);
            etConfirmNewPassword.setError(null);
            return true;
        }
        return false;
    }
}
