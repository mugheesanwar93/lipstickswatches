package com.lipstickswatches.android.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.SnapHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.adapter.BrandFilterAdapter;
import com.lipstickswatches.android.adapter.HorizontalListAdapter;
import com.lipstickswatches.android.adapter.TypeFilterAdapter;
import com.lipstickswatches.android.db.pojo.Brands;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.db.pojo.Types;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.pojo.ProductResponse.Datum;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.ApplicationController;
import com.lipstickswatches.android.utils.CustomImageView;
import com.lipstickswatches.android.utils.FileUtils;
import com.lipstickswatches.android.utils.SharedPrefUtils;
import com.lipstickswatches.android.utils.alphabatical_recyclerview.IndexFastScrollRecyclerView;
import com.tzutalin.dlib.Constants;
import com.tzutalin.dlib.FaceDet;
import com.tzutalin.dlib.VisionDetRet;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PhotoViewer extends BaseActivitiy implements View.OnClickListener, TextWatcher {
    private List<Products> productList;
    private List<Brands> brandList;
    private List<Types> typeList;
    private ImageButton btnBack, btnShare, btnDownload, btnCart, btnFilter;
    private LinearLayout container;
    private IndexFastScrollRecyclerView list;
    private HorizontalListAdapter adapter;
    private CustomImageView exImg;
    private Products product;
    private String productUrl;
    private Bitmap originalBitmapFromFilePath;
    private List<VisionDetRet> faceDetectResults;
    private boolean isLipstickAdded = false;
    private EditText etSearch;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);
        init();


    }

    private void init() {
        String filePath;
        if (Intent.ACTION_SEND.equals(getIntent().getAction()) && getIntent().getType() != null) {
            Uri imageUri = getIntent().getParcelableExtra(Intent.EXTRA_STREAM);
            filePath = AppUtil.getRealPathFromURI(this, imageUri);
//            Log.e("FILEPATH", "FilePath: " + filePath);

        } else {
            filePath = getIntent().getStringExtra("file_path");
        }
        if (filePath != null && filePath.trim().length() > 0) {
            brandList = ApplicationController.getDatabaseObj().brandsDAO().getAllBrands();
            typeList = ApplicationController.getDatabaseObj().typesDAO().getAllTypes();
            product = getIntent().getParcelableExtra("product");
            btnBack = findViewById(R.id.btn_back);
            btnFilter = findViewById(R.id.btn_filter);
            btnShare = findViewById(R.id.btn_share);
            btnDownload = findViewById(R.id.btn_download);
            btnCart = findViewById(R.id.btn_cart);
            container = findViewById(R.id.container);
            etSearch = findViewById(R.id.etSearch);
            etSearch.addTextChangedListener(this);
            list = findViewById(R.id.ivList);
            list.setIndexBarColor(R.color.colorPrimary);
            list.setPreviewColor(R.color.colorPrimary);
            LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            list.setLayoutManager(manager);
            SnapHelper snapHelper = new LinearSnapHelper();
            snapHelper.attachToRecyclerView(list);
            btnBack.setOnClickListener(this);
            btnFilter.setOnClickListener(this);
            btnShare.setOnClickListener(this);
            btnDownload.setOnClickListener(this);
            btnCart.setOnClickListener(this);

            if (ApplicationController.getDatabaseObj().productDAO().getAllProducts().size() < 0) {
                callService(filePath);
            } else {
                showProgress();
                productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts();
                adapter = new HorizontalListAdapter(productList, this);
                list.setAdapter(adapter);
                detectFace(filePath);
            }
//            mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());
        } else {
            AppUtil.showToast(this, "No image found");
        }

        loadAds();

    }

    public void detectFace(String filePath) {
        originalBitmapFromFilePath = BitmapFactory.decodeFile(filePath);
        new PhotoTask().execute();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_share:
                if (SharedPrefUtils.readString("save_img_link", "").trim().length() > 0) {
                    AppUtil.shareImage(SharedPrefUtils.readString("save_img_link", ""), PhotoViewer.this);
                } else {
                    if (product != null) {
                        if (isLipstickAdded) {
                            new SaveImage().execute(product.getId());
                            AppUtil.shareImage(SharedPrefUtils.readString("save_img_link", ""), PhotoViewer.this);
                        } else {
                            AppUtil.showToast(this, "Please apply lipstick before sharing image.");
                        }
                    } else {
                        AppUtil.showToast(this, getString(R.string.select_lipstick));
                    }
                }

                break;
            case R.id.btn_download:
                if (product != null) {
                    if (isLipstickAdded) {
                        new SaveImage().execute(product.getId());
                    } else {
                        AppUtil.showToast(this, "Please apply lipstick before saving image.");
                    }

                } else {
                    AppUtil.showToast(this, getString(R.string.select_lipstick));
                }
                break;

            case R.id.btn_cart:
                if (productUrl != null && !productUrl.equals("")) {
                    AppUtil.openLink(productUrl, this);
                } else {
                    AppUtil.showToast(this, getString(R.string.select_lipstick));
                }
                break;

            case R.id.btn_filter:
                final AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.CustomDialogTheme);
                final View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_filter_dialog, null);

                final Spinner spBrand = dialogView.findViewById(R.id.spBrand);
                final Spinner spType = dialogView.findViewById(R.id.spType);
                Button bFilter = dialogView.findViewById(R.id.bFilter);

                dialog.setView(dialogView);
                final AlertDialog show = dialog.show();

                if (brandList != null && brandList.size() > 0) {
                    BrandFilterAdapter brandFilterAdapter = new BrandFilterAdapter(getApplicationContext(), R.layout.holder_brands, brandList);
                    spBrand.setAdapter(brandFilterAdapter);
                }

                if (typeList != null && typeList.size() > 0) {
                    TypeFilterAdapter typeFilterAdapter = new TypeFilterAdapter(getApplicationContext(), R.layout.holder_brands, typeList);
                    spType.setAdapter(typeFilterAdapter);
                }

                bFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int brandId = brandList.get(spBrand.getSelectedItemPosition()).getId();
                        int typeId = typeList.get(spType.getSelectedItemPosition()).getId();
                        if (brandId == -1 && typeId == -1) {
                            productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts();
                        } else if (brandId == -1) {
                            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredProductsByType(String.valueOf(typeId));
                        } else if (typeId == -1) {
                            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredProductsByBrand(String.valueOf(brandId));
                        } else {
                            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredProducts(String.valueOf(brandId), String.valueOf(typeId));
                        }
                        adapter = new HorizontalListAdapter(productList, PhotoViewer.this);
                        list.setAdapter(adapter);
                        show.dismiss();
                    }
                });
                break;
        }
    }

    private void callService(final String filePath) {
        showProgress();
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));

        NetworkManager.getInstance().post("get_all_products", params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {

                switch (response) {
                    case com.lipstickswatches.android.utils.Constants.WebApi.Response.NO_INTERNET:
                        dismissProgress();
                        AppUtil.showToast(PhotoViewer.this, getString(R.string.no_internet));
                        break;
                    case com.lipstickswatches.android.utils.Constants.WebApi.Response.NOT_FOUND:
                        dismissProgress();
                        AppUtil.showToast(PhotoViewer.this, getString(R.string.no_products));
                        break;
                    case com.lipstickswatches.android.utils.Constants.WebApi.Response.SUCCESS:
                        Gson gson = new Gson();
                        ArrayList<Datum> newProductList = gson.fromJson((String) object, new TypeToken<ArrayList<Datum>>() {
                        }.getType());

                        AppUtil.removeProducts();
                        AppUtil.saveProducts(newProductList);

                        productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts();
                        dismissProgress();
                        adapter = new HorizontalListAdapter(productList, PhotoViewer.this);
                        list.setAdapter(adapter);
                        detectFace(filePath);
                        break;
                }
            }
        });


    }

    public void refreshFace(Products product) {
        drawImageOnImageView(faceDetectResults, product);
    }

    public void updateProduct(Products product) {
        this.product = product;
    }

    public void updateCartUrl(String url) {
        productUrl = url;
    }

    // all drawings are being made following points from this image:
    // https://www.pyimagesearch.com/wp-content/uploads/2017/04/facial_landmarks_68markup-1024x825.jpg
    private void drawFaceAnnotations(Canvas canvas, List<VisionDetRet> mFaces, String lipColor) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setPathEffect(new CornerPathEffect(50));
        float[] direction = new float[]{1, 10, 1};
        float ambient = 1f;
        float specular = 1f;
        float blurRadius = 10;
        paint.setMaskFilter(new EmbossMaskFilter(direction, ambient, specular, blurRadius));

        Path upperLipsPath = new Path();
        Path lowerLipsPath = new Path();

        for (final VisionDetRet ret : mFaces) {
            ArrayList<Point> landmarks = ret.getFaceLandmarks();

//            Log.e("faceview", "total num of landmarks points " + landmarks.size());
            int i = 0;
            // finding the extreme left (starting point) of the upper lips
            int[] extremeLeftPartOfUpperLip = new int[2];
            // hashmap to store the upper lips' bottom portion for later use
            HashMap<String, Integer[]> upperLipsArrayForReverse = new HashMap<>();
            // hashmap to store the lower lips' upper portion for later use
            HashMap<String, Integer[]> lowerLipsArrayForReverse = new HashMap<>();
            int offset = 10;
            int onset = 5;
            for (Point point : landmarks) {
                switch (i) {
                    case 48:
                        // start upper lips and store the extreme left points
                        upperLipsPath.moveTo(point.x, point.y);
                        extremeLeftPartOfUpperLip[0] = point.x;
                        extremeLeftPartOfUpperLip[1] = point.y;
                        break;
                    case 49://-5
                        upperLipsPath.lineTo((point.x - onset), (point.y - onset));
                        break;
                    case 50://10
                        upperLipsPath.lineTo((point.x - offset), (point.y - offset));
                        break;
                    case 51:
                        upperLipsPath.lineTo((point.x - offset), (point.y - offset));
                        break;
                    case 52:
                        upperLipsPath.lineTo((point.x - offset), (point.y - offset));
                        break;
                    case 53:
                        upperLipsPath.lineTo((point.x - onset), (point.y - onset));
                        break;
                    case 54:
                        // last point (extreme right) of the upper lips
                        upperLipsPath.lineTo(point.x, point.y);
                        // start the lower lips from this point
                        lowerLipsPath.moveTo(point.x, point.y);
                        break;
                    case 55:
                        lowerLipsPath.lineTo(point.x, point.y);
                        break;
                    case 56:
                        lowerLipsPath.lineTo(point.x, point.y);
                        break;
                    case 57:
                        lowerLipsPath.lineTo(point.x, point.y);
                        break;
                    case 58:
                        lowerLipsPath.lineTo(point.x, point.y);
                        break;
                    case 59:
                        lowerLipsPath.lineTo(point.x, point.y);
                        // move the line to extreme left of the upper lips to join them
                        lowerLipsPath.lineTo(extremeLeftPartOfUpperLip[0], extremeLeftPartOfUpperLip[1]);
                        break;
                    // store upper & lower lips inner points to use them later
                    case 60:
                        upperLipsArrayForReverse.put(String.valueOf(i), new Integer[]{point.x, point.y});
                        lowerLipsArrayForReverse.put(String.valueOf(i), new Integer[]{point.x, point.y});
                        break;
                    case 61:
                        upperLipsArrayForReverse.put(String.valueOf(i), new Integer[]{point.x, point.y});
                        break;
                    case 62:
                        upperLipsArrayForReverse.put(String.valueOf(i), new Integer[]{point.x, point.y});
                        break;
                    case 63:
                        upperLipsArrayForReverse.put(String.valueOf(i), new Integer[]{point.x, point.y});
                        break;
                    case 64:
                        upperLipsArrayForReverse.put(String.valueOf(i), new Integer[]{point.x, point.y});
                        lowerLipsArrayForReverse.put(String.valueOf(i), new Integer[]{point.x, point.y});
                        break;
                    case 65:
                        lowerLipsArrayForReverse.put(String.valueOf(i), new Integer[]{point.x, point.y});
                        break;
                    case 66:
                        lowerLipsArrayForReverse.put(String.valueOf(i), new Integer[]{point.x, point.y});
                        break;
                    case 67:
                        lowerLipsArrayForReverse.put(String.valueOf(i), new Integer[]{point.x, point.y});
                        break;
                }
                i++;
            }
            // start drawing upper lips' lower area by reversing path
            Integer[] uVal1 = upperLipsArrayForReverse.get("64");
            upperLipsPath.lineTo(uVal1[0], uVal1[1]);
            Integer[] uVal2 = upperLipsArrayForReverse.get("63");
            upperLipsPath.lineTo(uVal2[0], uVal2[1]);
            Integer[] uVal3 = upperLipsArrayForReverse.get("62");
            upperLipsPath.lineTo(uVal3[0], uVal3[1]);
            Integer[] uVal4 = upperLipsArrayForReverse.get("60");
            upperLipsPath.lineTo(uVal4[0], uVal4[1]);
            upperLipsPath.close();

            // start drawing lower lips' upper area by reversing path
            Integer[] lVal1 = lowerLipsArrayForReverse.get("60");
            lowerLipsPath.lineTo(lVal1[0], lVal1[1]);
            Integer[] lVal2 = lowerLipsArrayForReverse.get("67");
            lowerLipsPath.lineTo(lVal2[0], lVal2[1]);
            Integer[] lVal3 = lowerLipsArrayForReverse.get("66");
            lowerLipsPath.lineTo(lVal3[0], lVal3[1]);
            Integer[] lVal4 = lowerLipsArrayForReverse.get("65");
            lowerLipsPath.lineTo(lVal4[0], lVal4[1]);
            Integer[] lVal6 = lowerLipsArrayForReverse.get("64");
            lowerLipsPath.lineTo(lVal6[0], lVal6[1]);
            lowerLipsPath.close();

            //fix here
            if (lipColor != null && lipColor.trim().length() > 0) {
//                String tmpColor = lipColor;
                // set paint and style
                if (!lipColor.substring(0, 3).equals("#70")) {
                    lipColor = new StringBuffer(lipColor).insert(1, "70").toString();
                }
                paint.setColor(Color.parseColor(lipColor));
                paint.setStyle(Paint.Style.FILL);
                paint.setStrokeJoin(Paint.Join.ROUND);
                paint.setStrokeWidth(1);
                // draw both paths here to form a mouth
                canvas.drawPath(upperLipsPath, paint);
                canvas.drawPath(lowerLipsPath, paint);
                isLipstickAdded = true;
//                lipColor = tmpColor;
            } else {
                isLipstickAdded = false;
            }
        }
    }


    private void drawImageOnImageView(List<VisionDetRet> results, Products product) {
        exImg.setImageDrawable(null);
        Bitmap tempBitmap = Bitmap.createBitmap(originalBitmapFromFilePath.getWidth(), originalBitmapFromFilePath.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas tempCanvas = new Canvas(tempBitmap);

        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        tempCanvas.drawBitmap(originalBitmapFromFilePath, 0, 0, paint);
        if (product != null) {
            drawFaceAnnotations(tempCanvas, results, product.getColorCode());
        } else {
            drawFaceAnnotations(tempCanvas, results, null);
        }
        exImg.setImageDrawable(new BitmapDrawable(getResources(), tempBitmap));
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        adapter.getFilter().filter(s);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private class PhotoTask extends AsyncTask<Void, Void, Boolean> {


        FaceDet faceDet;

        @Override
        protected Boolean doInBackground(Void... voids) {

            final String targetPath = Constants.getFaceShapeModelPath();
            if (!new File(targetPath).exists()) {
                FileUtils.copyFileFromRawToOthers(PhotoViewer.this, R.raw.sp, targetPath);
            }
            faceDet = new FaceDet(Constants.getFaceShapeModelPath());
            if (originalBitmapFromFilePath != null) {
                faceDetectResults = faceDet.detect(originalBitmapFromFilePath);
            } else {
                faceDetectResults = new ArrayList<>();
            }
            return faceDetectResults.size() > 0;
        }

        @Override
        protected void onPostExecute(Boolean s) {
            super.onPostExecute(s);
            exImg = findViewById(R.id.custom_face_view);
            if (s) {
                if (product != null) {
                    productUrl = product.getDownloadLink();
                    drawImageOnImageView(faceDetectResults, product);
                    list.scrollToPosition(adapter.getItemPositionbyId(product.getId()));
                } else {
                    drawImageOnImageView(faceDetectResults, null);
                }
                container.setVisibility(View.VISIBLE);
                btnCart.setVisibility(View.VISIBLE);
                btnDownload.setVisibility(View.VISIBLE);
                btnShare.setVisibility(View.VISIBLE);
                btnFilter.setVisibility(View.VISIBLE);
            } else {
                AppUtil.showToast(PhotoViewer.this, "We couldn't detect any face in this picture, Please choose another one");
                btnCart.setVisibility(View.GONE);
                btnDownload.setVisibility(View.GONE);
                btnShare.setVisibility(View.GONE);
                btnFilter.setVisibility(View.GONE);
            }
            faceDet.release();
            dismissProgress();
        }
    }

    private class SaveImage extends AsyncTask<Integer, Void, Boolean> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(PhotoViewer.this, null, getResources().getString(R.string.loading));
            dialog.setCancelable(false);
        }

        @Override
        protected Boolean doInBackground(Integer... integers) {
            BitmapDrawable draw = (BitmapDrawable) exImg.getDrawable();
            Bitmap bitmap = draw.getBitmap();
            Bitmap bitmapWithWatermark = AppUtil.addWatermark(getResources(), bitmap);
            File file = FileUtils.getOutputMediaFile2(integers[0]);
            try {
                bitmapWithWatermark.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(file));
                SharedPrefUtils.writeString("save_img_link", file.getAbsolutePath());
                MediaScannerConnection.scanFile(PhotoViewer.this, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
//                        Log.e("ExternalStorage", "Scanned " + path + ":");
//                        Log.e("ExternalStorage", "-> uri=" + uri);
                    }
                });
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;

            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dialog.dismiss();
            if (aBoolean) {
                AppUtil.showToast(PhotoViewer.this, "Image Saved!!");

            } else {
                AppUtil.showToast(PhotoViewer.this, "Something went wrong while saving image");
            }
        }
    }

    private void loadAds() {
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();//.addTestDevice("CB120A73743E36FAECCAD807FDB46D29")
        mAdView.loadAd(adRequest);
    }

}
