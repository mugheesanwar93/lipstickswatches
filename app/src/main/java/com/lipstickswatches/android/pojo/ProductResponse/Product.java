package com.lipstickswatches.android.pojo.ProductResponse;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mugheesanwar on 26/03/2018.
 */

public class Product implements Parcelable {
    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("color_name")
    @Expose
    private String colorName;
    @SerializedName("color_code")
    @Expose
    private String colorCode;
    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("download_link")
    @Expose
    private String downloadLink;
    @SerializedName("designer_id")
    @Expose
    private String designerId;
    @SerializedName("type_id")
    @Expose
    private String typeId;
    @SerializedName("brand_id")
    @Expose
    private String brandId;
    @SerializedName("is_discount")
    @Expose
    private String isDiscount;
    @SerializedName("discount_code")
    @Expose
    private String discountCode;
    @SerializedName("favorited_count")
    @Expose
    private String favoritedCount;
    @SerializedName("designer")
    @Expose
    private Designer designer;
    @SerializedName("type")
    @Expose
    private Type type;
    @SerializedName("brand")
    @Expose
    private Brand brand;
    @SerializedName("is_favorite_by_user")
    @Expose
    private Integer isFavoriteByUser;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    protected Product(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        colorName = in.readString();
        colorCode = in.readString();
        downloadLink = in.readString();
        designerId = in.readString();
        typeId = in.readString();
        brandId = in.readString();
        isDiscount = in.readString();
        discountCode = in.readString();
        favoritedCount = in.readString();
        if (in.readByte() == 0) {
            isFavoriteByUser = null;
        } else {
            isFavoriteByUser = in.readInt();
        }
        createdAt = in.readString();
        updatedAt = in.readString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public String getDesignerId() {
        return designerId;
    }

    public void setDesignerId(String designerId) {
        this.designerId = designerId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getIsDiscount() {
        return isDiscount;
    }

    public void setIsDiscount(String isDiscount) {
        this.isDiscount = isDiscount;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public String getFavoritedCount() {
        return favoritedCount;
    }

    public void setFavoritedCount(String favoritedCount) {
        this.favoritedCount = favoritedCount;
    }

    public Designer getDesigner() {
        return designer;
    }

    public void setDesigner(Designer designer) {
        this.designer = designer;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Integer getIsFavoriteByUser() {
        return isFavoriteByUser;
    }

    public void setIsFavoriteByUser(Integer isFavoriteByUser) {
        this.isFavoriteByUser = isFavoriteByUser;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        parcel.writeString(name);
        parcel.writeString(colorName);
        parcel.writeString(colorCode);
        parcel.writeString(downloadLink);
        parcel.writeString(designerId);
        parcel.writeString(typeId);
        parcel.writeString(brandId);
        parcel.writeString(isDiscount);
        parcel.writeString(discountCode);
        parcel.writeString(favoritedCount);
        if (isFavoriteByUser == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(isFavoriteByUser);
        }

        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
    }
}
