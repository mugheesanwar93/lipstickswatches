package com.lipstickswatches.android.pojo;

public class GalleryImages {
    private String imagePath;
    private boolean isSelected = false;

    public GalleryImages(String imagePath, boolean isSelected) {
        this.imagePath = imagePath;
        this.isSelected = isSelected;
    }

    public String getImagePath() {
        return imagePath;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
