package com.lipstickswatches.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.db.pojo.RecentViewed;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.ui.MainContainerActivity;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.ApplicationController;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.SharedPrefUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by mugheesanwar on 21/03/2018.
 */

public class ListsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private List<Products> productList;
    private List<Products> searchList;
    private Context context;
    private boolean isMainList;
    private boolean isNewlyAddedList = false;
    private boolean isRecentViewedList = false;


    public ListsAdapter(List<Products> productList, Context context, boolean isMainList) {
        this.productList = productList;
        this.searchList = productList;
        this.context = context;
        this.isMainList = isMainList;

    }

    public ListsAdapter(List<Products> productList, Context context, boolean isMainList, boolean isNewlyAddedList) {
        this.productList = productList;
        this.searchList = productList;
        this.context = context;
        this.isMainList = isMainList;
        this.isNewlyAddedList = isNewlyAddedList;
    }

    public ListsAdapter(List<Products> productList, Context context, boolean isMainList, boolean isNewlyAddedList, boolean isRecentViewedList) {
        this.productList = productList;
        this.searchList = productList;
        this.context = context;
        this.isMainList = isMainList;
        this.isNewlyAddedList = isNewlyAddedList;
        this.isRecentViewedList = isRecentViewedList;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_lists, parent, false);
        return new ListsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        ListsHolder hldr = (ListsHolder) holder;
        hldr.ivColor.setColorFilter(Color.parseColor(productList.get(position).getColorCode()));
        hldr.name.setText(productList.get(position).getName());
        hldr.ivColorName.setText(productList.get(position).getColorName());
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.parseColor(productList.get(position).getColorCode()));
        drawable.setStroke(2, context.getResources().getColor(R.color.border));
        drawable.setShape(GradientDrawable.OVAL);
        hldr.ivCircle.setImageDrawable(drawable);
        if (productList.get(position).getIsFavoriteByUser().equals(1)) {
            hldr.cbFavorite.setChecked(true);
        } else {
            hldr.cbFavorite.setChecked(false);
        }


    }

    @Override
    public int getItemCount() {
        return (null != productList ? productList.size() : 0);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                productList = (ArrayList<Products>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Products> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = searchList;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }
                FilterResults results = new Filter.FilterResults();
                results.values = filteredResults;
                return results;
            }
        };
    }

    private ArrayList<Products> getFilteredResults(String constraint) {
        ArrayList<Products> results = new ArrayList<>();

        for (Products product : searchList) {
            if (product.getName().toLowerCase().contains(constraint)) {
                results.add(product);
            } else if (product.getColorName().toLowerCase().contains(constraint)) {
                results.add(product);
            }
        }
        return results;
    }


    public class ListsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public Button bTryItOn;
        public ImageView ivColor, ivCircle;
        public TextView name, ivColorName;
        public LinearLayout ll;
        public ImageButton ibCart;
        public CheckBox cbFavorite;

        public ListsHolder(View itemView) {
            super(itemView);
            bTryItOn = itemView.findViewById(R.id.bTryItOn);
            ivColor = itemView.findViewById(R.id.ivColor);
            ivCircle = itemView.findViewById(R.id.ivCircle);
            name = itemView.findViewById(R.id.name);
            ivColorName = itemView.findViewById(R.id.ivColorName);
            ll = itemView.findViewById(R.id.ll);
            ibCart = itemView.findViewById(R.id.ibCart);
            cbFavorite = itemView.findViewById(R.id.cbFavourite);
            bTryItOn.setOnClickListener(this);
            ibCart.setOnClickListener(this);
            cbFavorite.setOnClickListener(this);
            ll.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.bTryItOn:
                    if (isNewlyAddedList || isRecentViewedList) {
                        Intent intent = new Intent(context, MainContainerActivity.class);
                        intent.putExtra("product", productList.get(getAdapterPosition()));
                        intent.putExtra("disable_other_buttons", true);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent();
                        intent.setAction(Constants.AppIntents.ACTION_OPEN_LIPSTICKFRAGMENT);
                        intent.putExtra("product", productList.get(getAdapterPosition()));
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                    }
                    break;
                case R.id.ibCart:
                    AppUtil.openLink(productList.get(getAdapterPosition()).getDownloadLink(), context);
                    break;
                case R.id.cbFavourite:
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));
                    params.put("product_id", String.valueOf(productList.get(getAdapterPosition()).getId()));
                    params.put("status", String.valueOf(cbFavorite.isChecked() ? 1 : 0));
                    NetworkManager.getInstance().post("favorite_unfavorite_product", params, new RequestResponseListener<Object>() {
                        @Override
                        public void onResult(Integer response, Object object) {
                            switch (response) {
                                case Constants.WebApi.Response.NO_INTERNET:
                                    AppUtil.showToast(context, context.getString(R.string.no_internet));
                                    break;
                                case Constants.WebApi.Response.NOT_FOUND:
                                    AppUtil.showToast(context, context.getString(R.string.username_not_found));
                                    break;
                                case Constants.WebApi.Response.SUCCESS:
                                    try {
                                        JSONArray jb = new JSONArray((String) object);
                                        JSONObject jObj = jb.getJSONObject(0);
                                        productList.get(getAdapterPosition()).setIsFavoriteByUser(jObj.getInt("is_fav"));
                                        ApplicationController.getDatabaseObj().productDAO().updateProductById(jObj.getInt("is_fav"), String.valueOf(productList.get(getAdapterPosition()).getId()));
                                        Intent intent = new Intent();
                                        if (isMainList) {
                                            intent.setAction(Constants.AppIntents.ACTION_REFRESH_TRENDING_LISTS);
                                        } else {
                                            intent.setAction(Constants.AppIntents.ACTION_REFRESH_MAIN_LISTS);
                                        }
                                        intent.putExtra("product", productList.get(getAdapterPosition()));
                                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    break;
                                case Constants.WebApi.Response.TIMEOUT:
                                    AppUtil.showToast(context, context.getString(R.string.no_internet));
                                    break;
                                case Constants.WebApi.Response.UNAUTHORIZED:
                                    AppUtil.showToast(context, context.getString(R.string.account_suspended));
                                    break;
                            }
                        }
                    });
                    break;

                case R.id.ll:

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);

                    View dialogView = LayoutInflater.from(context).inflate(R.layout.fragment_lipstick_information, null);
                    builder.setView(dialogView);

                    LinearLayout llDiscount = dialogView.findViewById(R.id.llDiscount);
                    View view_ = dialogView.findViewById(R.id.view);

                    ImageView ivColor = dialogView.findViewById(R.id.ivColor);
                    ImageView ivCircle = dialogView.findViewById(R.id.ivCircle);
                    TextView tvName = dialogView.findViewById(R.id.tvName);
                    TextView tvBrand = dialogView.findViewById(R.id.tvBrand);
                    TextView tvColorName = dialogView.findViewById(R.id.tvColorName);
                    TextView tvType = dialogView.findViewById(R.id.tvType);
                    TextView tvDiscount = dialogView.findViewById(R.id.tvDiscount);
                    TextView tvDetail = dialogView.findViewById(R.id.tvDetail);

                    ImageButton ibCart = dialogView.findViewById(R.id.ibCart);
                    ImageButton ibCross = dialogView.findViewById(R.id.ibCross);
                    ImageButton ibShare = dialogView.findViewById(R.id.ibShare);
                    CheckBox cbFavourite = dialogView.findViewById(R.id.cbFavourite);
                    Button bTryItOn = dialogView.findViewById(R.id.bTryItOn);

                    ivColor.setColorFilter(Color.parseColor(productList.get(getAdapterPosition()).getColorCode()));
                    tvName.setText(productList.get(getAdapterPosition()).getName());
                    tvBrand.setText(productList.get(getAdapterPosition()).getBrandName());
                    tvColorName.setText(productList.get(getAdapterPosition()).getColorName());
                    tvColorName.setTextColor(Color.parseColor(productList.get(getAdapterPosition()).getColorCode()));

                    tvType.setText(productList.get(getAdapterPosition()).getTypeName());
                    tvDiscount.setText(productList.get(getAdapterPosition()).getIsDiscount());

                    GradientDrawable drawable = new GradientDrawable();
                    drawable.setColor(Color.parseColor(productList.get(getAdapterPosition()).getColorCode()));
                    drawable.setStroke(2, context.getResources().getColor(R.color.border));
                    drawable.setShape(GradientDrawable.OVAL);
                    ivCircle.setImageDrawable(drawable);

                    AssetManager am = context.getAssets();
                    Typeface typeface1 = Typeface.createFromAsset(am,
                            String.format(Locale.US, "fonts/%s", "app_font.ttf"));
                    Typeface typeface2 = Typeface.createFromAsset(am,
                            String.format(Locale.US, "fonts/%s", "antonio.ttf"));

                    tvDetail.setTypeface(typeface1);
                    tvBrand.setTypeface(typeface2);

                    ibCart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AppUtil.openLink(productList.get(getAdapterPosition()).getDownloadLink(), context);
                        }
                    });
                    final AlertDialog dialog = builder.create();
                    dialog.show();
                    if (!isRecentViewedList) {
                        ApplicationController.getDatabaseObj().recentViewedDAO().insertRecentViewed(new RecentViewed(productList.get(getAdapterPosition()).getId(), new Date()));
                    }
                    bTryItOn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            if (isNewlyAddedList || isRecentViewedList) {
                                Intent intent = new Intent(context, MainContainerActivity.class);
                                intent.putExtra("product", productList.get(getAdapterPosition()));
                                intent.putExtra("disable_other_buttons", true);
                                context.startActivity(intent);
                            } else {
                                Intent intent = new Intent();
                                intent.setAction(Constants.AppIntents.ACTION_OPEN_LIPSTICKFRAGMENT);
                                intent.putExtra("product", productList.get(getAdapterPosition()));
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                            }
                        }
                    });

                    ibCross.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    ibShare.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AppUtil.shareContent(productList.get(getAdapterPosition()).getName()
                                            + " by " +
                                            productList.get(getAdapterPosition()).getBrandName() + "\n" +
                                            "Get this lipstick from:\n" + productList.get(getAdapterPosition()).getDownloadLink(),
                                    context);
                        }
                    });

                    if (productList.get(getAdapterPosition()).getIsFavoriteByUser().equals(1)) {
                        cbFavourite.setChecked(true);
                    }

                    if (productList.get(getAdapterPosition()).getIsDiscount().equals("0")) {
                        llDiscount.setVisibility(View.GONE);
                        view_.setVisibility(View.GONE);
                    } else {
                        llDiscount.setVisibility(View.VISIBLE);
                        view_.setVisibility(View.VISIBLE);
                        tvDiscount.setText(productList.get(getAdapterPosition()).getDiscountCode());
                    }

                    cbFavourite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            Map<String, String> params = new HashMap<>();
                            params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));
                            params.put("product_id", String.valueOf(productList.get(getAdapterPosition()).getId()));
                            params.put("status", String.valueOf(isChecked ? 1 : 0));
                            NetworkManager.getInstance().post("favorite_unfavorite_product", params, new RequestResponseListener<Object>() {
                                @Override
                                public void onResult(Integer response, Object object) {
                                    switch (response) {
                                        case Constants.WebApi.Response.NO_INTERNET:
                                            AppUtil.showToast(context, context.getString(R.string.no_internet));
                                            break;
                                        case Constants.WebApi.Response.NOT_FOUND:
                                            AppUtil.showToast(context, context.getString(R.string.username_not_found));
                                            break;
                                        case Constants.WebApi.Response.SUCCESS:

                                            try {
                                                JSONArray jb = new JSONArray((String) object);
                                                JSONObject jObj = jb.getJSONObject(0);
                                                productList.get(getAdapterPosition()).setIsFavoriteByUser(jObj.getInt("is_fav"));
                                                ApplicationController.getDatabaseObj().productDAO().updateProductById(jObj.getInt("is_fav"), String.valueOf(productList.get(getAdapterPosition()).getId()));

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            notifyItemChanged(getAdapterPosition());
                                            Intent intent = new Intent();
                                            if (isMainList) {
                                                intent.setAction(Constants.AppIntents.ACTION_REFRESH_TRENDING_LISTS);
                                            } else {
                                                intent.setAction(Constants.AppIntents.ACTION_REFRESH_MAIN_LISTS);
                                            }
                                            intent.putExtra("product", productList.get(getAdapterPosition()));
                                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                                            break;
                                        case Constants.WebApi.Response.TIMEOUT:
                                            AppUtil.showToast(context, context.getString(R.string.no_internet));
                                            break;
                                        case Constants.WebApi.Response.UNAUTHORIZED:
                                            AppUtil.showToast(context, context.getString(R.string.account_suspended));
                                            break;
                                    }
                                }
                            });
                        }
                    });
                    break;
            }
        }
    }


}
