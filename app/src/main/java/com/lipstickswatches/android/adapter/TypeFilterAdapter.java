package com.lipstickswatches.android.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.db.pojo.Types;

import java.util.List;

public class TypeFilterAdapter extends ArrayAdapter<Types> {
    private Context mContext;
    private List<Types> typesList;

    public TypeFilterAdapter(@NonNull Context context, int resource, @NonNull List<Types> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.typesList = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        Types type = typesList.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.holder_brands, parent, false);
        }
//        LinearLayout ll = convertView.findViewById(R.id.ll);
        TextView tvBrandName = convertView.findViewById(R.id.tvBrandName);

        tvBrandName.setText(type.getName());
//        if (position % 2 == 0) {
//            ll.setBackgroundColor(mContext.getResources().getColor(R.color.list2));
//        } else {
//            ll.setBackgroundColor(mContext.getResources().getColor(R.color.list1));
//        }
        return convertView;
    }
}
