package com.lipstickswatches.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.lipstickswatches.android.R;

public class SliderViewpagerAdapter extends PagerAdapter {
    private Context mContext;
    private int[] mResources = new int[]{
            R.drawable.ls1, R.drawable.ls2, R.drawable.ls3,
            R.drawable.ls4, R.drawable.ls5, R.drawable.ls6,
            R.drawable.ls7, R.drawable.ls8, R.drawable.ls9,
            R.drawable.ls10, R.drawable.ls11, R.drawable.ls12,
            R.drawable.ls13, R.drawable.ls14, R.drawable.ls15,
            R.drawable.ls16, R.drawable.ls17, R.drawable.ls18,
            R.drawable.ls19, R.drawable.ls20, R.drawable.ls21
    };

    public SliderViewpagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((ImageView) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setImageResource(mResources[position]);
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}
