package com.lipstickswatches.android.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.pojo.GalleryImages;
import com.lipstickswatches.android.ui.LipstickDetailsActivity;
import com.lipstickswatches.android.ui.LipstickSwatchesActivity;
import com.lipstickswatches.android.utils.AppUtil;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

public class LipstickSwatchesAdapter extends RecyclerView.Adapter<LipstickSwatchesAdapter.LipstickSwatchesHolder> {

    private ArrayList<GalleryImages> imagePaths;
    private Activity context;
    private boolean isMultiSelectOn = false;

    public LipstickSwatchesAdapter(Activity mContext, ArrayList<GalleryImages> imagePaths) {
        this.context = mContext;
        this.imagePaths = imagePaths;
    }

    @NonNull
    @Override
    public LipstickSwatchesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_lipstick_swatches, parent, false);
        return new LipstickSwatchesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LipstickSwatchesHolder holder, final int position) {
        Picasso.get().load(new File(imagePaths.get(position).getImagePath())).into(holder.ivImages);
//        holder.ivImages.setImageURI(Uri.parse(imagePaths.get(position).getImagePath()));
        if (imagePaths.get(position).isSelected()) {
            holder.imageChecked.setVisibility(View.VISIBLE);
        } else {
            holder.imageChecked.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return (null != imagePaths ? imagePaths.size() : 0);
    }

    public ArrayList<GalleryImages> getSelectedImages() {
        ArrayList<GalleryImages> selectedImages = new ArrayList<>();
        for (GalleryImages images : imagePaths) {
            if (images.isSelected()) {
                selectedImages.add(images);
            }
        }
        return selectedImages;
    }

    public void removeFromMainList(GalleryImages item) {
        imagePaths.remove(item);
//        notifyDataSetChanged();
    }

    class LipstickSwatchesHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private ImageView ivImages, imageChecked;

        public LipstickSwatchesHolder(View itemView) {
            super(itemView);
            ivImages = itemView.findViewById(R.id.ivImages);
            imageChecked = itemView.findViewById(R.id.imageChecked);
            ivImages.setOnClickListener(this);
            ivImages.setOnLongClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.ivImages) {
                if (isMultiSelectOn) {
                    if (imagePaths.get(getAdapterPosition()).isSelected()) {
                        imagePaths.get(getAdapterPosition()).setSelected(false);
                        if (getSelectedImages().size() <= 0) {
                            ((LipstickSwatchesActivity) context).hideDeleteButton();
                            isMultiSelectOn = false;
                        }
                    } else {
                        imagePaths.get(getAdapterPosition()).setSelected(true);
                    }
                    notifyDataSetChanged();
                } else {
                    try {
                        String[] separated = imagePaths.get(getAdapterPosition()).getImagePath().split("/");
                        String productId = AppUtil.getProductIdFromName(separated[5]);
                        Intent i = new Intent(context, LipstickDetailsActivity.class);
                        i.putExtra("productId", productId);
                        i.putExtra("imageUri", imagePaths.get(getAdapterPosition()).getImagePath());
                        context.startActivityForResult(i, 1000);
                    } catch (Exception e) {
                        AppUtil.showToast(context, context.getString(R.string.something_wrong_with_image));
                    }
                }
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (v.getId() == R.id.ivImages) {
                if (!isMultiSelectOn) {
                    isMultiSelectOn = true;
                    ((LipstickSwatchesActivity) context).showDeleteButton();
                    imagePaths.get(getAdapterPosition()).setSelected(true);
                    notifyDataSetChanged();
                    return true;
                }
            }
            return false;
        }
    }
}
