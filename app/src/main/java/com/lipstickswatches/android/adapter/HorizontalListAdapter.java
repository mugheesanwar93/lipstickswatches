package com.lipstickswatches.android.adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.ButtonBarLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.ui.PhotoViewer;
import com.lipstickswatches.android.utils.AppUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HorizontalListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements SectionIndexer, Filterable {
    private List<Products> productList;
    private List<Products> searchList;
    private Context context;
    private ArrayList<Integer> mSectionPositions;

    public HorizontalListAdapter(List<Products> productList, Context context) {
        this.productList = productList;
        this.searchList = productList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_list_adapter, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ListViewHolder) {
            ListViewHolder hldr = (ListViewHolder) holder;
            hldr.ivColor.setColorFilter(Color.parseColor(productList.get(position).getColorCode()));
            hldr.name.setText(productList.get(position).getName());
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>();
        mSectionPositions = new ArrayList<>();
        for (int i = 0; i < productList.size(); i++) {
            String section = String.valueOf(productList.get(i).getName().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);

    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    public int getItemPositionbyId(Integer id) {
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getId().equals(id)) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                productList = (ArrayList<Products>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Products> filteredResults;
                if (constraint.length() == 0) {
                    filteredResults = searchList;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }
                FilterResults results = new Filter.FilterResults();
                results.values = filteredResults;
                return results;
            }
        };
    }

    protected ArrayList<Products> getFilteredResults(String constraint) {
        ArrayList<Products> results = new ArrayList<>();

        for (Products product : searchList) {
            if (product.getName().toLowerCase().contains(constraint)) {
                results.add(product);
            }
        }
        return results;
    }

    private class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private LinearLayout ll;
        private ImageView ivColor;
        private TextView name;

        public ListViewHolder(View itemView) {
            super(itemView);
            ll = itemView.findViewById(R.id.ll);
            ivColor = itemView.findViewById(R.id.ivColor);
            name = itemView.findViewById(R.id.name);
            ll.setOnClickListener(this);
            ll.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ll:
                    ((PhotoViewer) view.getContext()).refreshFace(productList.get(getAdapterPosition()));
                    ((PhotoViewer) view.getContext()).updateProduct(productList.get(getAdapterPosition()));
                    ((PhotoViewer) view.getContext()).updateCartUrl(productList.get(getAdapterPosition()).getDownloadLink());
                    break;
            }
        }

        @Override
        public boolean onLongClick(View view) {
            switch (view.getId()) {
                case R.id.ll:
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);

                    View dialogView = LayoutInflater.from(context).inflate(R.layout.fragment_lipstick_information, null);
                    builder.setView(dialogView);

                    LinearLayout llDiscount = dialogView.findViewById(R.id.llDiscount);
                    View view_ = dialogView.findViewById(R.id.view);

                    ImageView ivColor = dialogView.findViewById(R.id.ivColor);
                    TextView tvName = dialogView.findViewById(R.id.tvName);
                    TextView tvBrand = dialogView.findViewById(R.id.tvBrand);
                    TextView tvColorName = dialogView.findViewById(R.id.tvColorName);
                    TextView tvType = dialogView.findViewById(R.id.tvType);
                    TextView tvDiscount = dialogView.findViewById(R.id.tvDiscount);
                    TextView tvDetail = dialogView.findViewById(R.id.tvDetail);
                    ButtonBarLayout bblMain = dialogView.findViewById(R.id.bblMain);
                    bblMain.setVisibility(View.GONE);

                    ImageButton ibCross = dialogView.findViewById(R.id.ibCross);
                    ImageButton ibShare = dialogView.findViewById(R.id.ibShare);
                    ivColor.setColorFilter(Color.parseColor(productList.get(getAdapterPosition()).getColorCode()));
                    tvName.setText(productList.get(getAdapterPosition()).getName());
                    tvBrand.setText(productList.get(getAdapterPosition()).getBrandName());
                    tvColorName.setText(productList.get(getAdapterPosition()).getColorName());
                    tvColorName.setTextColor(Color.parseColor(productList.get(getAdapterPosition()).getColorCode()));

                    tvType.setText(productList.get(getAdapterPosition()).getTypeName());
                    tvDiscount.setText(productList.get(getAdapterPosition()).getIsDiscount());

                    AssetManager am = context.getAssets();

                    Typeface typeface1 = Typeface.createFromAsset(am,
                            String.format(Locale.US, "fonts/%s", "app_font.ttf"));
                    Typeface typeface2 = Typeface.createFromAsset(am,
                            String.format(Locale.US, "fonts/%s", "antonio.ttf"));

                    tvDetail.setTypeface(typeface1);
                    tvBrand.setTypeface(typeface2);

                    final AlertDialog dialog = builder.create();
                    dialog.show();

                    ibCross.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    ibShare.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AppUtil.shareContent(productList.get(getAdapterPosition()).getName()
                                            + " by " +
                                            productList.get(getAdapterPosition()).getBrandName() + "\n" +
                                            "Get this lipstick from:\n" + productList.get(getAdapterPosition()).getDownloadLink(),
                                    context);
                        }
                    });

                    if (productList.get(getAdapterPosition()).getIsDiscount().equals("0")) {
                        llDiscount.setVisibility(View.GONE);
                        view_.setVisibility(View.GONE);
                    } else {
                        llDiscount.setVisibility(View.VISIBLE);
                        view_.setVisibility(View.VISIBLE);
                        tvDiscount.setText(productList.get(getAdapterPosition()).getDiscountCode());
                    }
                    break;
            }
            return true;
        }
    }
}
