package com.lipstickswatches.android.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by mugheesanwar on 15/11/2016.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> allFrags;
    private ArrayList<String> title;
    private boolean withTitle = false;

    public ViewPagerAdapter(FragmentManager fm, ArrayList<Fragment> allFrags, boolean withTitle) {
        super(fm);
        this.allFrags = allFrags;
        this.withTitle = withTitle;

    }

    @Override
    public Fragment getItem(int position) {
        return allFrags.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (withTitle) {
            title = new ArrayList<>();
            title.add("All Products");
            title.add("Trending");

            return title.get(position);
        } else {
            return super.getPageTitle(position);
        }

    }

    @Override
    public int getCount() {
        return allFrags.size();
    }
}
