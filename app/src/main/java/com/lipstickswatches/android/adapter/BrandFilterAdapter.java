package com.lipstickswatches.android.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.db.pojo.Brands;

import java.util.List;

public class BrandFilterAdapter extends ArrayAdapter<Brands> {
    private Context mContext;
    private List<Brands> brandsList;

    public BrandFilterAdapter(@NonNull Context context, int resource, @NonNull List<Brands> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.brandsList = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        Brands brand = brandsList.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.holder_brands, parent, false);
        }
//        LinearLayout ll = convertView.findViewById(R.id.ll);
        TextView tvBrandName = convertView.findViewById(R.id.tvBrandName);

        tvBrandName.setText(brand.getName());
//        if (position % 2 == 0) {
//            ll.setBackgroundColor(mContext.getResources().getColor(R.color.list1));
//        } else {
//            ll.setBackgroundColor(mContext.getResources().getColor(R.color.list2));
//        }
        return convertView;
    }
}
