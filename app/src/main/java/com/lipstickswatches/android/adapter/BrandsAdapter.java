package com.lipstickswatches.android.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.fragments.ListFragment;
import com.lipstickswatches.android.pojo.ProductResponse.Brand;

import java.util.ArrayList;

public class BrandsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Brand> brandList;
    ListFragment listFragment;

    public BrandsAdapter(ArrayList<Brand> brandList, ListFragment fragment) {
        this.brandList = brandList;
        listFragment = fragment;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_brands, parent, false);
        return new BrandsHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BrandsHolder hldr = (BrandsHolder) holder;
        hldr.tvBrandName.setText(brandList.get(position).getName());

        if (position % 2 == 0) {
            hldr.ll.setBackgroundColor(listFragment.getResources().getColor(R.color.list1));
        } else {
            hldr.ll.setBackgroundColor(listFragment.getResources().getColor(R.color.list2));
        }

    }

    @Override
    public int getItemCount() {
        return (null != brandList ? brandList.size() : 0);

    }

    public class BrandsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public LinearLayout ll;
        public TextView tvBrandName;

        public BrandsHolder(View itemView) {
            super(itemView);

            ll = itemView.findViewById(R.id.ll);
            tvBrandName = itemView.findViewById(R.id.tvBrandName);
            ll.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.ll) {
                listFragment.updateList(String.valueOf(brandList.get(getAdapterPosition()).getId()));
                listFragment.dismissDialog();
            }
        }
    }
}
