package com.lipstickswatches.android.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.lipstickswatches.android.db.DAO.BrandsDAO;
import com.lipstickswatches.android.db.DAO.ProductsDAO;
import com.lipstickswatches.android.db.DAO.RecentViewedDAO;
import com.lipstickswatches.android.db.DAO.TypesDAO;
import com.lipstickswatches.android.db.pojo.Brands;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.db.pojo.RecentViewed;
import com.lipstickswatches.android.db.pojo.Types;

import static com.lipstickswatches.android.db.AppDatabase.DATABASE_VERSION;

@Database(entities = {Products.class, Brands.class, Types.class, RecentViewed.class}, version = DATABASE_VERSION, exportSchema = false)
@TypeConverters({DateConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public static final int DATABASE_VERSION = 5;

    public abstract ProductsDAO productDAO();

    public abstract BrandsDAO brandsDAO();

    public abstract TypesDAO typesDAO();

    public abstract RecentViewedDAO recentViewedDAO();
}
