package com.lipstickswatches.android.db.pojo;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity
public class Products implements Parcelable {

    public static final Creator<Products> CREATOR = new Creator<Products>() {
        @Override
        public Products createFromParcel(Parcel in) {
            return new Products(in);
        }

        @Override
        public Products[] newArray(int size) {
            return new Products[size];
        }
    };
    @PrimaryKey
    @ColumnInfo(name = "id")
    private Integer id;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "color_name")
    private String colorName;
    @ColumnInfo(name = "color_code")
    private String colorCode;
    @ColumnInfo(name = "status_id")
    private Integer statusId;
    @ColumnInfo(name = "download_link")
    private String downloadLink;
    @ColumnInfo(name = "designer_id")
    private String designerId;
    @ColumnInfo(name = "brand_id")
    private String brandId;
    @ColumnInfo(name = "type_id")
    private String typeId;
    @ColumnInfo(name = "is_discount")
    private String isDiscount;
    @ColumnInfo(name = "discount_code")
    private String discountCode;
    @ColumnInfo(name = "favorited_count")
    private String favoritedCount;
    @ColumnInfo(name = "designer_name")
    private String designerName;
    @ColumnInfo(name = "brand_name")
    private String brandName;
    @ColumnInfo(name = "type_name")
    private String typeName;
    @ColumnInfo(name = "is_favorite_by_user")
    private Integer isFavoriteByUser;
    @ColumnInfo(name = "created_at")
    private String createdAt;
    @ColumnInfo(name = "updated_at")
    private String updatedAt;

    public Products(Integer id,
                    String name,
                    String colorName,
                    String colorCode,
                    Integer statusId,
                    String downloadLink,
                    String designerId,
                    String brandId,
                    String typeId,
                    String isDiscount,
                    String discountCode,
                    String favoritedCount,
                    String designerName,
                    String brandName,
                    String typeName,
                    Integer isFavoriteByUser,
                    String createdAt,
                    String updatedAt) {
        this.id = id;
        this.name = name;
        this.colorName = colorName;
        this.colorCode = colorCode;
        this.statusId = statusId;
        this.downloadLink = downloadLink;
        this.designerId = designerId;
        this.brandId = brandId;
        this.typeId = typeId;
        this.isDiscount = isDiscount;
        this.discountCode = discountCode;
        this.favoritedCount = favoritedCount;
        this.designerName = designerName;
        this.typeName = typeName;
        this.brandName = brandName;
        this.isFavoriteByUser = isFavoriteByUser;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    protected Products(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        colorName = in.readString();
        colorCode = in.readString();
        if (in.readByte() == 0) {
            statusId = null;
        } else {
            statusId = in.readInt();
        }
        downloadLink = in.readString();
        designerId = in.readString();
        brandId = in.readString();
        typeId = in.readString();
        isDiscount = in.readString();
        discountCode = in.readString();
        favoritedCount = in.readString();
        designerName = in.readString();
        brandName = in.readString();
        typeName = in.readString();
        if (in.readByte() == 0) {
            isFavoriteByUser = null;
        } else {
            isFavoriteByUser = in.readInt();
        }
        createdAt = in.readString();
        updatedAt = in.readString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public String getDesignerId() {
        return designerId;
    }

    public void setDesignerId(String designerId) {
        this.designerId = designerId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getIsDiscount() {
        return isDiscount;
    }

    public void setIsDiscount(String isDiscount) {
        this.isDiscount = isDiscount;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public String getFavoritedCount() {
        return favoritedCount;
    }

    public void setFavoritedCount(String favoritedCount) {
        this.favoritedCount = favoritedCount;
    }

    public String getDesignerName() {
        return designerName;
    }

    public void setDesignerName(String designerName) {
        this.designerName = designerName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Integer getIsFavoriteByUser() {
        return isFavoriteByUser;
    }

    public void setIsFavoriteByUser(Integer isFavoriteByUser) {
        this.isFavoriteByUser = isFavoriteByUser;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        parcel.writeString(name);
        parcel.writeString(colorName);
        parcel.writeString(colorCode);
        if (statusId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(statusId);
        }
        parcel.writeString(downloadLink);
        parcel.writeString(designerId);
        parcel.writeString(brandId);
        parcel.writeString(typeId);
        parcel.writeString(isDiscount);
        parcel.writeString(discountCode);
        parcel.writeString(favoritedCount);
        parcel.writeString(designerName);
        parcel.writeString(brandName);
        parcel.writeString(typeName);
        if (isFavoriteByUser == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(isFavoriteByUser);
        }
        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
    }
}
