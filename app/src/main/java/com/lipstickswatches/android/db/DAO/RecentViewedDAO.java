package com.lipstickswatches.android.db.DAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.db.pojo.RecentViewed;

import java.util.List;

@Dao
public interface RecentViewedDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRecentViewed(RecentViewed recentViewed);

    @Query("SELECT * FROM Products INNER JOIN RecentViewed ON Products.id = RecentViewed.product_id GROUP BY RecentViewed.product_id")
    List<Products> getRecentViewedProducts();

    @Query("DELETE FROM RecentViewed")
    void deleteRecentViewed();
}
