package com.lipstickswatches.android.db.DAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;


import com.lipstickswatches.android.db.pojo.Types;

import java.util.List;

@Dao
public interface TypesDAO {
    //Types Query
    @Query("SELECT * FROM Types")
    List<Types> getAllTypes();

    @Insert
    void insertTypes(List<Types> brands);

    @Query("DELETE FROM Types")
    void deleteTypes();
}
