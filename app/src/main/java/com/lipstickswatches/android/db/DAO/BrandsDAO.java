package com.lipstickswatches.android.db.DAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.lipstickswatches.android.db.pojo.Brands;

import java.util.List;

@Dao
public interface BrandsDAO {
    //Brands Query
    @Query("SELECT * FROM Brands")
    List<Brands> getAllBrands();

    @Insert
    void insertBrands(List<Brands> brands);

    @Query("DELETE FROM Brands")
    void deleteBrands();
}
