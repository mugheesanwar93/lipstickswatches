package com.lipstickswatches.android.db.DAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.lipstickswatches.android.db.pojo.Products;

import java.util.List;

@Dao
public interface ProductsDAO {

    //All the required queries required for the functionalities

    @Query("SELECT * FROM Products ORDER BY name ASC")
    List<Products> getAllProducts();

    @Query("SELECT * FROM Products LIMIT :limit OFFSET :offset")
    List<Products> getAllProducts(int offset, int limit);

    @Insert
    void insertMultipleListRecord(List<Products> products);

    @Query("DELETE FROM Products")
    void delete();

    @Query("SELECT * FROM PRODUCTS WHERE id = :id")
    Products getProductById(String id);

    @Query("UPDATE Products SET is_favorite_by_user = :isFavoriteByUser WHERE id = :id")
    void updateProductById(Integer isFavoriteByUser, String id);

    @Query("SELECT * FROM Products WHERE is_favorite_by_user = 1")
    List<Products> getFavoritedProducts();

    @Query("SELECT * FROM Products ORDER BY favorited_count desc")
    List<Products> getTrendingProducts();

    @Query("SELECT * FROM Products ORDER BY favorited_count desc LIMIT :limit OFFSET :offset")
    List<Products> getTrendingProducts(int offset, int limit);

    @Query("SELECT * FROM Products WHERE brand_id =:brandId AND type_id=:typeId")
    List<Products> getFilteredProducts(String brandId, String typeId);

    @Query("SELECT * FROM Products WHERE brand_id =:brandId AND type_id=:typeId LIMIT :limit OFFSET :offset")
    List<Products> getFilteredProducts(String brandId, String typeId, int offset, int limit);

    @Query("SELECT * FROM Products WHERE type_id =:typeId")
    List<Products> getFilteredProductsByType(String typeId);

    @Query("SELECT * FROM Products WHERE type_id =:typeId LIMIT :limit OFFSET :offset")
    List<Products> getFilteredProductsByType(String typeId, int offset, int limit);

    @Query("SELECT * FROM Products WHERE brand_id =:brandId")
    List<Products> getFilteredProductsByBrand(String brandId);

    @Query("SELECT * FROM Products WHERE brand_id =:brandId LIMIT :limit OFFSET :offset")
    List<Products> getFilteredProductsByBrand(String brandId, int offset, int limit);

    @Query("SELECT * FROM Products WHERE brand_id =:brandId AND type_id=:typeId ORDER BY favorited_count desc")
    List<Products> getFilteredTrendingProducts(String brandId, String typeId);

    @Query("SELECT * FROM Products WHERE brand_id =:brandId AND type_id=:typeId ORDER BY favorited_count desc LIMIT :limit OFFSET :offset")
    List<Products> getFilteredTrendingProducts(String brandId, String typeId, int offset, int limit);
    @Query("SELECT * FROM Products WHERE type_id =:typeId ORDER BY favorited_count desc")
    List<Products> getFilteredTrendingProductsByType(String typeId);

    @Query("SELECT * FROM Products WHERE type_id =:typeId ORDER BY favorited_count desc LIMIT :limit OFFSET :offset")
    List<Products> getFilteredTrendingProductsByType(String typeId, int offset, int limit);

    @Query("SELECT * FROM Products WHERE brand_id =:brandId ORDER BY favorited_count desc")
    List<Products> getFilteredTrendingProductsByBrand(String brandId);

    @Query("SELECT * FROM Products WHERE brand_id =:brandId ORDER BY favorited_count desc LIMIT :limit OFFSET :offset")
    List<Products> getFilteredTrendingProductsByBrand(String brandId, int offset, int limit);

    @Query("SELECT * FROM Products WHERE created_at BETWEEN datetime('now','start of month') AND datetime('now','localtime') ORDER BY created_at DESC")
    List<Products> getNewlyAddedProducts();
}
