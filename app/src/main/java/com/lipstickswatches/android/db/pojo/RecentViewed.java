package com.lipstickswatches.android.db.pojo;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

@Entity
public class RecentViewed implements Parcelable {

    public static final Creator<RecentViewed> CREATOR = new Creator<RecentViewed>() {
        @Override
        public RecentViewed createFromParcel(Parcel in) {
            return new RecentViewed(in);
        }

        @Override
        public RecentViewed[] newArray(int size) {
            return new RecentViewed[size];
        }
    };
    @PrimaryKey
    @ColumnInfo(name = "id")
    private Integer id;
    @ColumnInfo(name = "product_id")
    private Integer productId;
    @ColumnInfo(name = "viewed_time")
    private Date viewedTime;


    public RecentViewed(Integer productId, Date viewedTime) {

        this.productId = productId;
        this.viewedTime = viewedTime;
    }

    protected RecentViewed(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            productId = null;
        } else {
            productId = in.readInt();
        }
        viewedTime = new Date(in.readString());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {

        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Date getViewedTime() {
        return viewedTime;
    }

    public void setViewedTime(Date viewedTime) {
        this.viewedTime = viewedTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (productId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(productId);
        }
        dest.writeString(viewedTime.toString());
    }

}
