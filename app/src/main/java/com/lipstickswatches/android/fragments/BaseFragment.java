package com.lipstickswatches.android.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.utils.ApplicationController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by mugheesanwar on 29/11/2017.
 */

public class BaseFragment extends Fragment {
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    /**
     * This function is used to transition from one activity to another.
     *
     * @param current     Current activity from which we are transitioning
     * @param newActivity Activity class to which we are transitioning
     * @param isFinished  if we want to finish current Activity or not
     */
    protected void transitionActivities(Activity current, Class newActivity, boolean isFinished) {
        Intent intent = new Intent(current, newActivity);
        startActivity(intent);
        if (isFinished) {
            current.finish();
        }
    }

    /**
     * This function is used to transition from one activity to another.
     *
     * @param current     Current activity from which we are transitioning
     * @param newActivity Activity class to which we are transitioning
     * @param isFinished  if we want to finish current Activity or not
     * @param flags       list of Intent flags
     */
    protected void transitionActivitiesFlags(Activity current, Class newActivity, boolean isFinished, List<Integer> flags) {
        Intent intent = new Intent(current, newActivity);
        if (flags != null && flags.size() > 0) {
            for (Integer flag :
                    flags) {
                intent.addFlags(flag);
            }
        }
        startActivity(intent);
        if (isFinished) {
            current.finish();
        }
    }


    /**
     * This method is used to show ProgressDialog. It dismisses if there is any previous instance running and then initialize dialog again.
     */

    protected void showProgress(Context context) {
        dismissProgress();

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    /**
     * This function dismisses if progress dialog is running and clear from memory.
     */

    protected void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public File saveBitmap(Bitmap bitmap) {
        File file = null;
        if (bitmap != null) {
            File myDir = new File("/sdcard/lipstickSwatches/");
            myDir.mkdirs();
            file = new File(myDir, "profile_picture.jpg");
            try {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(file); //here is set your file path where you want to save or also here you can set file object directly
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream); // bitmap is your Bitmap instance, if you want to compress it you can compress reduce percentage
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.flush();
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    protected void showToast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
    }

    public static Context getStaticApplicationContext() {
        return ApplicationController.getInstance().getApplicationContext();
    }

}
