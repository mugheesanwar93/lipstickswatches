package com.lipstickswatches.android.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.adapter.ListsAdapter;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.pojo.ProductResponse.Datum;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.ApplicationController;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TrendingLipstickFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView rvTrendingList;
    SwipeRefreshLayout srlTrendingList;
    RadioGroup pageNumberContainer;
    ListsAdapter listsAdapter;
    List<Products> productList;
    int OFFSET = 0;
    int LIMIT = 30;
    AdView mAdView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_trending_lipsticks, container, false);
        return init(v);
    }

    private View init(View v) {
        rvTrendingList = v.findViewById(R.id.rvTrendingList);
        GridLayoutManager llm = new GridLayoutManager(getContext(), 2);
        rvTrendingList.setLayoutManager(llm);
        pageNumberContainer = v.findViewById(R.id.pageNumberContainer);
        srlTrendingList = v.findViewById(R.id.srlTrendingList);
        srlTrendingList.setOnRefreshListener(this);

        if (ApplicationController.getDatabaseObj().productDAO().getAllProducts().size() <= 0) {
            callService(true);
        } else {
            productList = ApplicationController.getDatabaseObj().productDAO().getTrendingProducts(OFFSET, LIMIT);
            listsAdapter = new ListsAdapter(productList, getContext(), false);
            rvTrendingList.setAdapter(listsAdapter);
            setUpPagination();
        }
        loadAds(v);
        return v;
    }

    private void callService(boolean isProgress) {
        OFFSET = 0;
        if (isProgress) {
            showProgress(getContext());
        }
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));

//        NetworkManager.getInstance().post("get_favorited_products", params, new RequestResponseListener<Object>() {
        NetworkManager.getInstance().post("get_all_products", params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
//            @Override
//            public void onResult(Integer response, Object object) {
                if (srlTrendingList.isRefreshing()) {
                    srlTrendingList.setRefreshing(false);
                }
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        dismissProgress();
                        AppUtil.showToast(getContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        dismissProgress();
                        AppUtil.showToast(getContext(), getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        dismissProgress();
                        Gson gson = new Gson();
                        ArrayList<Datum> newProductList = gson.fromJson((String) object, new TypeToken<ArrayList<Datum>>() {
                        }.getType());

                        AppUtil.removeProducts();
                        AppUtil.saveProducts(newProductList);

                        productList = ApplicationController.getDatabaseObj().productDAO().getTrendingProducts(OFFSET, LIMIT);
                        listsAdapter = new ListsAdapter(productList, getContext(), false);
                        rvTrendingList.setAdapter(listsAdapter);
                        setUpPagination();
                        break;
                }
            }
        });


    }

    public void updateLists(Products product) {

        if (productList != null && productList.size() > 0) {
            for (Products prd : productList) {
                if (prd.getId().equals(product.getId())) {
                    prd.setIsFavoriteByUser(product.getIsFavoriteByUser());
                    break;
                }

            }
            listsAdapter.notifyDataSetChanged();

        }


    }

    public void updateListByBrand(int brandId, int typeId) {
        if (brandId == -1 && typeId == -1) {
            productList = ApplicationController.getDatabaseObj().productDAO().getTrendingProducts(OFFSET, LIMIT);
        } else if (brandId == -1) {
            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredTrendingProductsByType(String.valueOf(typeId), OFFSET, LIMIT);
        } else if (typeId == -1) {
            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredTrendingProductsByBrand(String.valueOf(brandId), OFFSET, LIMIT);
        } else {
            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredTrendingProducts(String.valueOf(brandId), String.valueOf(typeId), OFFSET, LIMIT);
        }
        if (productList.size() > 0) {
            rvTrendingList.setVisibility(View.VISIBLE);
            listsAdapter = new ListsAdapter(productList, getContext(), true);
            rvTrendingList.setAdapter(listsAdapter);
            setUpPagination(brandId, typeId);
        } else {
            rvTrendingList.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        callService(false);
    }

    private void setUpPagination() {

        pageNumberContainer.removeAllViews();
        int size = ApplicationController.getDatabaseObj().productDAO().getTrendingProducts().size();
        if (size > 0) {
            for (int i = 0; i <= Math.floor(size / LIMIT); i++) {
                RadioButton rBtn = new RadioButton(getContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                rBtn.setLayoutParams(params);

                rBtn.setPadding(5, 5, 5, 5);
                rBtn.setButtonDrawable(null);
                rBtn.setText("" + (i + 1));
                rBtn.setTypeface(rBtn.getTypeface(), Typeface.BOLD);
                rBtn.setTextColor(getResources().getColorStateList(R.color.page_number_bg));
                rBtn.setId(i);
                int finalI = i;
                rBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        OFFSET = finalI * LIMIT;
                        productList = ApplicationController.getDatabaseObj().productDAO().getTrendingProducts(OFFSET, LIMIT);
                        listsAdapter = new ListsAdapter(productList, getContext(), true);
                        rvTrendingList.setAdapter(listsAdapter);
                    }
                });

                pageNumberContainer.addView(rBtn);
                if (i == 0) {
                    pageNumberContainer.clearCheck();
                    pageNumberContainer.check(i);
                }

            }
        }
    }

    private void setUpPagination(int brandId, int typeId) {

        pageNumberContainer.removeAllViews();
        int size = 0;
        if (brandId == -1 && typeId == -1) {
            size = ApplicationController.getDatabaseObj().productDAO().getTrendingProducts().size();
        } else if (brandId == -1) {
            size = ApplicationController.getDatabaseObj().productDAO().getFilteredTrendingProductsByType(String.valueOf(typeId)).size();
        } else if (typeId == -1) {
            size = ApplicationController.getDatabaseObj().productDAO().getFilteredTrendingProductsByBrand(String.valueOf(brandId)).size();
        } else {
            size = ApplicationController.getDatabaseObj().productDAO().getFilteredTrendingProducts(String.valueOf(brandId), String.valueOf(typeId)).size();
        }
        if (size > 0) {
            for (int i = 0; i <= Math.floor(size / LIMIT); i++) {
                RadioButton rBtn = new RadioButton(getContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                rBtn.setLayoutParams(params);

                rBtn.setPadding(5, 5, 5, 5);
                rBtn.setButtonDrawable(null);
                rBtn.setText("" + (i + 1));
                rBtn.setTypeface(rBtn.getTypeface(), Typeface.BOLD);
                rBtn.setTextColor(getResources().getColorStateList(R.color.page_number_bg));
                rBtn.setId(i);
                int finalI = i;
                rBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        OFFSET = finalI * LIMIT;
                        if (brandId == -1 && typeId == -1) {
                            productList = ApplicationController.getDatabaseObj().productDAO().getTrendingProducts(OFFSET, LIMIT);
                        } else if (brandId == -1) {
                            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredTrendingProductsByType(String.valueOf(typeId), OFFSET, LIMIT);
                        } else if (typeId == -1) {
                            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredTrendingProductsByBrand(String.valueOf(brandId), OFFSET, LIMIT);
                        } else {
                            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredTrendingProducts(String.valueOf(brandId), String.valueOf(typeId), OFFSET, LIMIT);
                        }
                        listsAdapter = new ListsAdapter(productList, getContext(), true);
                        rvTrendingList.setAdapter(listsAdapter);
                    }
                });

                pageNumberContainer.addView(rBtn);
                if (i == 0) {
                    pageNumberContainer.clearCheck();
                    pageNumberContainer.check(i);
                }

            }
        }
    }

    private void loadAds(View view) {
        mAdView = view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build(); //.addTestDevice("CB120A73743E36FAECCAD807FDB46D29")
        mAdView.loadAd(adRequest);
    }
}
