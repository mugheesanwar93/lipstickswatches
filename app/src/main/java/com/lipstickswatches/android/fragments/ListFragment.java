package com.lipstickswatches.android.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.adapter.BrandFilterAdapter;
import com.lipstickswatches.android.adapter.TypeFilterAdapter;
import com.lipstickswatches.android.adapter.ViewPagerAdapter;
import com.lipstickswatches.android.db.pojo.Brands;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.db.pojo.Types;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.pojo.ProductResponse.Brand;
import com.lipstickswatches.android.pojo.ProductResponse.Type;
import com.lipstickswatches.android.ui.SearchActivity;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.ApplicationController;
import com.lipstickswatches.android.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;


public class ListFragment extends BaseFragment implements View.OnClickListener {
    ViewPager vpMain;
    TabLayout tabs;

    TextView toolbar;
    ImageView ivFilter, ivSearch;

    List<Brands> brandList;
    List<Types> typeList;
    BrandFilterAdapter brandFilterAdapter;
    TypeFilterAdapter typeFilterAdapter;
    AlertDialog show = null;
    private MainLipsticksFragment mainListSticks;
    private TrendingLipstickFragment trendingLipstickFragment;
    private int typeSelectId = 0;
    private int brandSelectId = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lists, container, false);
        return init(view);
    }

    private View init(View view) {
        toolbar = view.findViewById(R.id.toolbar);
        AppUtil.changeToolbarFont(getContext(), toolbar);

        ivFilter = view.findViewById(R.id.ivFilter);
        ivFilter.setOnClickListener(this);

        ivSearch = view.findViewById(R.id.ivSearch);
        ivSearch.setOnClickListener(this);

        vpMain = view.findViewById(R.id.vpMain);
        tabs = view.findViewById(R.id.tabs);

        ArrayList<Fragment> allFrags = new ArrayList<>();
        mainListSticks = new MainLipsticksFragment();
        trendingLipstickFragment = new TrendingLipstickFragment();
        allFrags.add(mainListSticks);
        allFrags.add(trendingLipstickFragment);
        vpMain.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), allFrags, true));
        vpMain.setOffscreenPageLimit(0);
        tabs.setupWithViewPager(vpMain);

//        vpMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                if (position == 0) {
//                    ivFilter.setVisibility(View.VISIBLE);
//                    ivSearch.setVisibility(View.VISIBLE);
//                } else {
//                    ivFilter.setVisibility(View.INVISIBLE);
//                    ivSearch.setVisibility(View.INVISIBLE);
//                }
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });

        brandList = ApplicationController.getDatabaseObj().brandsDAO().getAllBrands();
        typeList = ApplicationController.getDatabaseObj().typesDAO().getAllTypes();

        if (brandList.size() == 0 || typeList.size() == 0) {
            callService();
        }

        return view;
    }

    private void callService() {
        NetworkManager.getInstance().get("get_all_brands", new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        dismissProgress();
                        AppUtil.showToast(getStaticApplicationContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        dismissProgress();
                        AppUtil.showToast(getStaticApplicationContext(), getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        Gson gson = new Gson();
                        ArrayList<Brand> newBrandList = gson.fromJson((String) object, new TypeToken<ArrayList<Brand>>() {
                        }.getType());
                        newBrandList.add(new Brand(
                                -1, "All", "1"
                        ));

                        AppUtil.removeBrands();
                        AppUtil.saveBrands(newBrandList);
                        brandList = ApplicationController.getDatabaseObj().brandsDAO().getAllBrands();

                        break;
                }
            }
        });

        NetworkManager.getInstance().get("get_all_types", new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        dismissProgress();
                        AppUtil.showToast(getStaticApplicationContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        dismissProgress();
                        AppUtil.showToast(getStaticApplicationContext(), getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        Gson gson = new Gson();
                        ArrayList<Type> newTypeList = gson.fromJson((String) object, new TypeToken<ArrayList<Type>>() {
                        }.getType());
                        newTypeList.add(new Type(
                                -1, "All", "1"
                        ));

                        AppUtil.removeTypes();
                        AppUtil.saveTypes(newTypeList);
                        typeList = ApplicationController.getDatabaseObj().typesDAO().getAllTypes();

                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivFilter:
                final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.CustomDialogTheme);
                final View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.custom_filter_dialog, null);
                TextView tvFilter = dialogView.findViewById(R.id.tvFilter);
                Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),
                        String.format(Locale.US, "fonts/%s", "app_font.ttf"));
                tvFilter.setTypeface(typeface);
                final Spinner spBrand = dialogView.findViewById(R.id.spBrand);
                final Spinner spType = dialogView.findViewById(R.id.spType);
                Button bFilter = dialogView.findViewById(R.id.bFilter);

                dialog.setView(dialogView);
                show = dialog.show();

                if (brandList != null && brandList.size() > 0) {
                    Collections.reverse(brandList);
                    brandFilterAdapter = new BrandFilterAdapter(getContext(), R.layout.holder_brands, brandList);
                    spBrand.setAdapter(brandFilterAdapter);
                    spBrand.setSelection(brandSelectId);
                }

                if (typeList != null && typeList.size() > 0) {
                    Collections.reverse(typeList);
                    typeFilterAdapter = new TypeFilterAdapter(getContext(), R.layout.holder_brands, typeList);
                    spType.setAdapter(typeFilterAdapter);
                    spType.setSelection(typeSelectId);
                }

                bFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        brandSelectId = spBrand.getSelectedItemPosition();
                        typeSelectId = spType.getSelectedItemPosition();
                        int brandId = brandList.get(spBrand.getSelectedItemPosition()).getId();
                        int typeId = typeList.get(spType.getSelectedItemPosition()).getId();
                        if (vpMain.getCurrentItem() == 0) {
                            mainListSticks.applyFilters(brandId, typeId);
                        } else {
                            trendingLipstickFragment.updateListByBrand(brandId, typeId);
                        }
                        dismissDialog();
                    }
                });
                break;

            case R.id.ivSearch:
                transitionActivities(getActivity(), SearchActivity.class, false);
                break;

        }
    }

    public void updateList(String brandID) {
        if (!brandID.equals("0")) {
            mainListSticks.callServiceByBrandId(brandID);
        } else {
            mainListSticks.callService(true);
        }
    }

    public void dismissDialog() {
        show.dismiss();
    }

    public void updateFragmentLists(int refreshWhichList, Products product) {
        switch (refreshWhichList) {
            case Constants.MAIN_LIST:
                if (mainListSticks != null) {
                    mainListSticks.updateList(product);
                }
                break;
            case Constants.TRENDING_LIST:
                if (trendingLipstickFragment != null) {
                    trendingLipstickFragment.updateLists(product);
                }
                break;
        }


    }

}
