package com.lipstickswatches.android.fragments;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.ui.FavoriteProductsActivity;
import com.lipstickswatches.android.ui.LipstickSwatchesActivity;
import com.lipstickswatches.android.ui.NewlyAddedActivity;
import com.lipstickswatches.android.ui.ProfileActivity;
import com.lipstickswatches.android.ui.RecentViewedActivity;
import com.lipstickswatches.android.utils.AppUtil;

import java.util.Locale;

public class HomeFragment extends BaseFragment implements View.OnClickListener {
    private ImageView ivMenu, ivInvite;
    private TextView toolbar, homeHeading, tv1, tv2, tv3, tv4;
    private LinearLayout btnSwatchFactors, btnNewlyAdded, btnFavorites, btnRecentViewed;
    private AdView mAdView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        return init(view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupToolbar();
        AssetManager am = getActivity().getAssets();
        Typeface typeface = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "heading_font.ttf"));
        Typeface typeface2 = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "antonio.ttf"));
        homeHeading.setTypeface(typeface, Typeface.BOLD);

        tv1.setTypeface(typeface2);
        tv2.setTypeface(typeface2);
        tv3.setTypeface(typeface2);
        tv4.setTypeface(typeface2);
        btnFavorites.setOnClickListener(this);
        btnSwatchFactors.setOnClickListener(this);
        btnNewlyAdded.setOnClickListener(this);
        btnRecentViewed.setOnClickListener(this);
    }

    private View init(View view) {
        ivMenu = view.findViewById(R.id.ivMenu);
        ivInvite = view.findViewById(R.id.ivInvite);
        toolbar = view.findViewById(R.id.toolbar);
        homeHeading = view.findViewById(R.id.homeHeading);
        tv1 = view.findViewById(R.id.tv1);
        tv2 = view.findViewById(R.id.tv2);
        tv3 = view.findViewById(R.id.tv3);
        tv4 = view.findViewById(R.id.tv4);
        btnFavorites = view.findViewById(R.id.btnFavorites);
        btnNewlyAdded = view.findViewById(R.id.btnNewlyAdded);
        btnRecentViewed = view.findViewById(R.id.btnRecentViewed);
        btnSwatchFactors = view.findViewById(R.id.btnSwatchFactors);
        loadAds(view);
        return view;
    }

    private void setupToolbar() {
        AppUtil.changeToolbarFont(getContext(), toolbar);
        ivMenu.setOnClickListener(this);
        ivInvite.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivMenu:
                transitionActivities(getActivity(), ProfileActivity.class, false);
                break;
            case R.id.ivInvite:
                AppUtil.invitePeople(getContext());
                break;
            case R.id.btnFavorites:
                transitionActivities(getActivity(), FavoriteProductsActivity.class, false);
                break;
            case R.id.btnNewlyAdded:
                transitionActivities(getActivity(), NewlyAddedActivity.class, false);
                break;
            case R.id.btnRecentViewed:
                transitionActivities(getActivity(), RecentViewedActivity.class, false);
                break;
            case R.id.btnSwatchFactors:
                transitionActivities(getActivity(), LipstickSwatchesActivity.class, false);
                break;
        }
    }
    private void loadAds(View view) {
        mAdView = view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build(); //.addTestDevice("CB120A73743E36FAECCAD807FDB46D29")
        mAdView.loadAd(adRequest);
    }
}
