package com.lipstickswatches.android.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.MediaActionSound;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.lipstickswatches.android.R;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.ui.PhotoViewer;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.CameraPreview;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class CameraFragment extends Fragment implements View.OnClickListener {
    private static final int RESULT_LOAD_IMAGE = 0001;
    private static final int PERMISSION_FLAG = 1;
    private static final String REQUIRED_PERMISSIONS[] = {
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private Camera mCamera;
    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    private FloatingActionButton btnCapture;
    private CheckBox btnSwitch;
    private ImageButton btnPhotoLib;
    private FrameLayout cameraPreview;
    private boolean cameraFront = true, isFlashOn = false;
    private View view;
    private Products product;
    private Bitmap updatedImage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_camera, container, false);
        if (getArguments() != null) {
            product = getArguments().getParcelable("product");
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requiredPermission(view);

        } else {
            init(view);
        }
        return view;
    }

    private void init(View view) {

        if (AppUtil.checkCameraHardware(getContext())) {
            initializeCamera(view);
        } else {
            AlertDialog.Builder d = new AlertDialog.Builder(getContext());
            d.setMessage("No Camera hardware found in this device");
            d.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            d.setCancelable(false);
            d.show();
        }

    }

    private void initializeCamera(View view) {
        btnCapture = view.findViewById(R.id.btn_capture);
        btnSwitch = view.findViewById(R.id.btn_switch);
        btnPhotoLib = view.findViewById(R.id.btn_photo_lib);
//        btnFlash = (ImageButton) findViewById(R.id.btn_flash);
        cameraPreview = view.findViewById(R.id.camera_preview);

        if (findFrontFacingCamera() < 0) {
            AppUtil.showToast(getContext(), "No front facing camera found.");
            btnSwitch.setVisibility(View.GONE);
            mCamera = Camera.open(findBackFacingCamera());
            setCameraDisplayOrientation(Camera.CameraInfo.CAMERA_FACING_BACK, mCamera);
            cameraFront = false;
        } else {
            mCamera = Camera.open(findFrontFacingCamera());
            setCameraDisplayOrientation(Camera.CameraInfo.CAMERA_FACING_FRONT, mCamera);
        }


        mPicture = getPictureCallback();
        mPreview = new CameraPreview(getContext(), mCamera);
//        mPreview.refreshCamera(mCamera, false);
        cameraPreview.addView(mPreview);
        setCamParams();

        btnSwitch.setOnClickListener(this);
        btnCapture.setOnClickListener(this);
        btnPhotoLib.setOnClickListener(this);
//        btnFlash.setOnClickListener(this);

    }

    private void setCamParams() {
        Camera.Parameters params = mCamera.getParameters();

        List<String> focusModes = params.getSupportedFocusModes();

        String CAF_PICTURE = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE,
                CAF_VIDEO = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO,
                supportedMode = focusModes
                        .contains(CAF_PICTURE) ? CAF_PICTURE : focusModes
                        .contains(CAF_VIDEO) ? CAF_VIDEO : "";

        if (!supportedMode.equals("")) {

            params.setFocusMode(supportedMode);
            mCamera.setParameters(params);

        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_capture:
                mCamera.takePicture(null, null, mPicture);
                break;
            case R.id.btn_switch:
                chooseCamera();
                break;

            case R.id.btn_photo_lib:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_LOAD_IMAGE);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK) {
            Uri returnUri = data.getData();
            CropImage
                    .activity(returnUri)
                    .setMinCropWindowSize(AppUtil.getDisplayWidth(getActivity()), AppUtil.getDisplayWidth(getActivity()))
                    .setGuidelines(CropImageView.Guidelines.ON)

                    .start(getContext(), CameraFragment.this);

        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                try {
                    updatedImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), resultUri);
                    File pictureFile = AppUtil.saveImage(updatedImage);
                    if (pictureFile != null) {
                        Intent intent = new Intent(getContext(), PhotoViewer.class);
                        intent.putExtra("file_path", pictureFile.getAbsolutePath());
                        intent.putExtra("product", product);
                        startActivity(intent);
                    } else {
                        AppUtil.showToast(getContext(), "Something went wrong while saving file, please try again.");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    AppUtil.showToast(getContext(), "Something went wrong while saving file, please try again.");
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void chooseCamera() {
        if (cameraFront) {
            int cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                mCamera.release();
                mCamera = null;
                mCamera = Camera.open(cameraId);
                setCamParams();
                setCameraDisplayOrientation(cameraId, mCamera);
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);

                cameraFront = false;
            }
        } else {
            int cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                mCamera.release();
                mCamera = null;
                mCamera = Camera.open(cameraId);
                setCameraDisplayOrientation(cameraId, mCamera);
                setCamParams();
                mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
                cameraFront = true;
            }
        }
    }

    private int findFrontFacingCamera() {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;
    }

    private int findBackFacingCamera() {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFront = false;
                break;
            }
        }
        return cameraId;
    }


    private Camera.PictureCallback getPictureCallback() {
        Camera.PictureCallback picture = new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(final byte[] data, Camera camera) {
                MediaActionSound sound = new MediaActionSound();
                sound.play(MediaActionSound.SHUTTER_CLICK);

                try {
                    Bitmap realImage = adjustImage(data);
                    CropImage
                            .activity(AppUtil.getImageUri(getContext(), realImage))
                            .setMinCropWindowSize(AppUtil.getDisplayWidth(getActivity()), AppUtil.getDisplayWidth(getActivity()))
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(getContext(), CameraFragment.this);
                } catch (IOException e) {
                    e.printStackTrace();
                    AppUtil.showToast(getContext(), "Something went wrong, please try again");
                }


//                final File pictureFile = FileUtils.getOutputMediaFile();
//
//                if (pictureFile == null) {
//                    return;
//                }
//
//                getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(pictureFile)));
//                MediaScannerConnection.scanFile(getContext(), new String[]{pictureFile.getAbsolutePath()}, null, new MediaScannerConnection.MediaScannerConnectionClient() {
//                    @Override
//                    public void onMediaScannerConnected() {
//
//                    }
//
//                    @Override
//                    public void onScanCompleted(final String s, Uri uri) {
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                FileOutputStream fos = null;
//                                try {
//                                    fos = new FileOutputStream(pictureFile);
//                                    BitmapFactory.Options options = new BitmapFactory.Options();
//                                    options.inJustDecodeBounds = true;
//                                    BitmapFactory.decodeByteArray(data, 0, data.length, options);
//                                    options.inSampleSize = AppUtil.calculateInSampleSize(options, 720, 1280);
//                                    options.inJustDecodeBounds = false;
//                                    Bitmap realImage = BitmapFactory.decodeByteArray(data, 0, data.length, options);
//                                    adjustPicture(pictureFile, fos, realImage);
////                                    Intent intent = new Intent(getContext(), PhotoViewer.class);
////                                    intent.putExtra("file_path", pictureFile.getAbsolutePath());
////                                    intent.putExtra("product", product);
////                                    startActivity(intent);
//                                } catch (FileNotFoundException e) {
//                                    e.printStackTrace();
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//
//                            }
//                        });
//                    }
//                });

//                mPreview.refreshCamera(mCamera, false);
            }
        };
        return picture;
    }

    private Bitmap adjustImage(byte[] data) throws IOException {
        ExifInterface exifInterface = new ExifInterface(new ByteArrayInputStream(data));
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                if (cameraFront) {
                    matrix.postRotate(-90);
                } else {
                    matrix.postRotate(90);
                }
                matrix.postScale(-1, 1);
                break;

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                matrix.postRotate(90);
                matrix.postScale(-1, 1);
                break;

        }
        Bitmap realImage = BitmapFactory.decodeByteArray(data, 0, data.length);

//        Bitmap scaledBitmap = Bitmap.createScaledBitmap(realImage, realImage.getWidth(), realImage.getHeight(), true);
        return Bitmap.createBitmap(realImage, 0, 0, realImage.getWidth(), realImage.getHeight(), matrix, true);
    }

    private void requiredPermission(View view) {
        ArrayList<String> permission = new ArrayList<>();
        for (String per : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(getContext(), per) != PackageManager.PERMISSION_GRANTED) {
                permission.add(per);
            }
        }
        if (permission.size() > 0) {
            requestPermissions(permission.toArray(new String[permission.size()]), PERMISSION_FLAG);
        } else {
            init(view);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_FLAG) {
            if (grantResults.length > 0) {
                boolean isAllGranted = true;
                for (int status : grantResults) {
                    if (status == PackageManager.PERMISSION_DENIED) {
                        isAllGranted = false;
                    }
                }
                if (isAllGranted) {
                    init(view);
                } else {
                    AppUtil.showToast(getContext(), "You need to allow all permissions to run this app");

                }
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    private void setCameraDisplayOrientation(int cameraId, Camera camera) {

        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;

        } else {
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
        camera.getParameters().setRotation(result);
    }

    private void adjustPicture(File pictureFile, FileOutputStream fos, Bitmap realImage) throws IOException {

//        ExifInterface exif = new ExifInterface(pictureFile.toString());
//        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//        Matrix matrix = new Matrix();
//        switch (orientation) {
//            case ExifInterface.ORIENTATION_ROTATE_90:
//                matrix.postRotate(90);
//                break;
//            case ExifInterface.ORIENTATION_ROTATE_180:
//                matrix.postRotate(180);
//                break;
//            case ExifInterface.ORIENTATION_ROTATE_270:
//                matrix.postRotate(270);
//                break;
//            case ExifInterface.ORIENTATION_UNDEFINED:
//                if (cameraFront) {
//                    matrix.postRotate(-90);
//                } else {
//                    matrix.postRotate(90);
//                }
//
//        }
//        realImage = Bitmap.createBitmap(realImage, 0, 0, realImage.getWidth(), realImage.getHeight(), matrix, true);
        CropImage.activity(AppUtil.getImageUri(getContext(), realImage)).start(getContext(), this);
//        updatedImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
//        fos.close();
//        return updatedImage;
    }
}
