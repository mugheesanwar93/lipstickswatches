package com.lipstickswatches.android.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.adapter.ListsAdapter;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.pojo.ProductResponse.Datum;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.ApplicationController;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainLipsticksFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView rvList;
    SwipeRefreshLayout srlList;
    ListsAdapter listsAdapter;
    RadioGroup pageNumberContainer;
    GridLayoutManager llm;
    List<Products> productList;
    int OFFSET = 0;
    int LIMIT = 30;
    AdView mAdView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main_lipsticks, container, false);
        return init(v);
    }

    private View init(View v) {

        rvList = v.findViewById(R.id.rvList);
        llm = new GridLayoutManager(getContext(), 2);
        rvList.setLayoutManager(llm);

        srlList = v.findViewById(R.id.srlList);
        srlList.setOnRefreshListener(this);

        pageNumberContainer = v.findViewById(R.id.pageNumberContainer);
        if (ApplicationController.getDatabaseObj().productDAO().getAllProducts().size() <= 0) {
            callService(true);
        } else {
            productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts(OFFSET, LIMIT);
            listsAdapter = new ListsAdapter(productList, getContext(), true);
            rvList.setAdapter(listsAdapter);
            setUpPagination();

        }
        loadAds(v);
        return v;
    }

    public void callService(boolean isProgress) {
        OFFSET = 0;
        if (isProgress) {
            showProgress(getContext());
        }
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));

        NetworkManager.getInstance().post("get_all_products", params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                if (srlList.isRefreshing()) {
                    srlList.setRefreshing(false);
                }
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        dismissProgress();
                        AppUtil.showToast(getContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        dismissProgress();
                        AppUtil.showToast(getContext(), getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        dismissProgress();
                        Gson gson = new Gson();
                        ArrayList<Datum> newProductList = gson.fromJson((String) object, new TypeToken<ArrayList<Datum>>() {
                        }.getType());

                        AppUtil.removeProducts();
                        AppUtil.saveProducts(newProductList);

                        productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts(OFFSET, LIMIT);
                        listsAdapter = new ListsAdapter(productList, getContext(), true);
                        rvList.setAdapter(listsAdapter);
                        setUpPagination();
                        break;
                }
            }
        });


    }

    public void callServiceByBrandId(String brandId) {
        OFFSET = 0;
        showProgress(getContext());
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));
        params.put("brand_id", brandId);

        NetworkManager.getInstance().post("get_brand_products", params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        dismissProgress();
                        AppUtil.showToast(getContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        dismissProgress();
                        AppUtil.showToast(getContext(), getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        Gson gson = new Gson();
                        ArrayList<Datum> newProductList = gson.fromJson((String) object, new TypeToken<ArrayList<Datum>>() {
                        }.getType());

                        AppUtil.removeProducts();
                        AppUtil.saveProducts(newProductList);

                        productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts(OFFSET, LIMIT);
                        dismissProgress();
                        listsAdapter = new ListsAdapter(productList, getContext(), true);
                        rvList.setAdapter(listsAdapter);
                        setUpPagination();
                        break;
                }
            }
        });

        if (srlList.isRefreshing()) {
            srlList.setRefreshing(false);
        }
    }


    public void updateList(Products product) {
        if (productList != null && productList.size() > 0) {
            for (Products prd : productList) {
                if (prd.getId().equals(product.getId())) {
                    prd.setIsFavoriteByUser(product.getIsFavoriteByUser());
                    break;
                }

            }
            listsAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onRefresh() {
        callService(false);
    }

    public void applyFilters(int brandId, int typeId) {
        OFFSET = 0;
        if (brandId == -1 && typeId == -1) {
            productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts(OFFSET, LIMIT);
        } else if (brandId == -1) {
            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredProductsByType(String.valueOf(typeId), OFFSET, LIMIT);
        } else if (typeId == -1) {
            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredProductsByBrand(String.valueOf(brandId), OFFSET, LIMIT);
        } else {
            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredProducts(String.valueOf(brandId), String.valueOf(typeId), OFFSET, LIMIT);
        }
        if (productList.size() > 0) {
            rvList.setVisibility(View.VISIBLE);
            listsAdapter = new ListsAdapter(productList, getContext(), true);
            rvList.setAdapter(listsAdapter);
            setUpPagination(brandId, typeId);
        } else {
            rvList.setVisibility(View.GONE);
            pageNumberContainer.setVisibility(View.GONE);
        }
    }

    private void setUpPagination() {

        pageNumberContainer.removeAllViews();
        int size = ApplicationController.getDatabaseObj().productDAO().getAllProducts().size();
        if (size > 0) {
            for (int i = 0; i <= Math.floor(size / LIMIT); i++) {
                RadioButton rBtn = new RadioButton(getContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                rBtn.setLayoutParams(params);

                rBtn.setPadding(5, 5, 5, 5);
                rBtn.setButtonDrawable(null);
                rBtn.setText("" + (i + 1));
                rBtn.setTypeface(rBtn.getTypeface(), Typeface.BOLD);
                rBtn.setTextColor(getResources().getColorStateList(R.color.page_number_bg));
                rBtn.setId(i);
                int finalI = i;
                rBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        OFFSET = finalI * LIMIT;
                        productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts(OFFSET, LIMIT);
                        listsAdapter = new ListsAdapter(productList, getContext(), true);
                        rvList.setAdapter(listsAdapter);
                    }
                });

                pageNumberContainer.addView(rBtn);
                if (i == 0) {
                    pageNumberContainer.clearCheck();
                    pageNumberContainer.check(i);
                }

            }
        }
    }

    private void setUpPagination(int brandId, int typeId) {

        pageNumberContainer.removeAllViews();
        int size = 0;
        if (brandId == -1 && typeId == -1) {
            size = ApplicationController.getDatabaseObj().productDAO().getAllProducts().size();
        } else if (brandId == -1) {
            size = ApplicationController.getDatabaseObj().productDAO().getFilteredProductsByType(String.valueOf(typeId)).size();
        } else if (typeId == -1) {
            size = ApplicationController.getDatabaseObj().productDAO().getFilteredProductsByBrand(String.valueOf(brandId)).size();
        } else {
            size = ApplicationController.getDatabaseObj().productDAO().getFilteredProducts(String.valueOf(brandId), String.valueOf(typeId)).size();
        }
        if (size > 0) {
            for (int i = 0; i <= Math.floor(size / LIMIT); i++) {
                RadioButton rBtn = new RadioButton(getContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                rBtn.setLayoutParams(params);

                rBtn.setPadding(5, 5, 5, 5);
                rBtn.setButtonDrawable(null);
                rBtn.setText("" + (i + 1));
                rBtn.setTypeface(rBtn.getTypeface(), Typeface.BOLD);
                rBtn.setTextColor(getResources().getColorStateList(R.color.page_number_bg));
                rBtn.setId(i);
                int finalI = i;
                rBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        OFFSET = finalI * LIMIT;
                        if (brandId == -1 && typeId == -1) {
                            productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts(OFFSET, LIMIT);
                        } else if (brandId == -1) {
                            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredProductsByType(String.valueOf(typeId), OFFSET, LIMIT);
                        } else if (typeId == -1) {
                            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredProductsByBrand(String.valueOf(brandId), OFFSET, LIMIT);
                        } else {
                            productList = ApplicationController.getDatabaseObj().productDAO().getFilteredProducts(String.valueOf(brandId), String.valueOf(typeId), OFFSET, LIMIT);
                        }
                        listsAdapter = new ListsAdapter(productList, getContext(), true);
                        rvList.setAdapter(listsAdapter);
                    }
                });

                pageNumberContainer.addView(rBtn);
                if (i == 0) {
                    pageNumberContainer.clearCheck();
                    pageNumberContainer.check(i);
                }

            }
        }
    }

    private void loadAds(View view) {
        mAdView = view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build(); //.addTestDevice("CB120A73743E36FAECCAD807FDB46D29")
        mAdView.loadAd(adRequest);
    }


//        rvList.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                if (dy > 0) {
//                    if (llm.findLastVisibleItemPosition() >= PAGE_SIZE) {
//                        count++;
//                        if (PAGE_SIZE < llm.getItemCount()) {
//                            PAGE_SIZE += 30;
//                        }
//                        pageNumberContainer.clearCheck();
//                        pageNumberContainer.check(count);
//                        Log.e("recyclerview", "Current last visible item position while going down: " + llm.findLastVisibleItemPosition());
//                        Log.e("recyclerview", "Current page size while going down: " + PAGE_SIZE);
//
//                    }
//                    if (!recyclerView.canScrollVertically(RecyclerView.VERTICAL)) {
//
//                        PAGE_SIZE = (llm.getItemCount() - (llm.getItemCount() % 10)) - 30;
//                        Log.e("recyclerview", "recycler view reached end, reseting PAGE_SIZE value to max = " + PAGE_SIZE);
//                    }
//                } else {
//
//                    if (llm.findFirstVisibleItemPosition() <= PAGE_SIZE) {
//                        Log.e("recyclerview", "Current last visible item position while going up: " + llm.findFirstVisibleItemPosition());
//                        Log.e("recyclerview", "Current page size while going up (before): " + PAGE_SIZE);
//                        count--;
//                        PAGE_SIZE -= 30;
//                        pageNumberContainer.clearCheck();
//                        pageNumberContainer.check(count);
//                        Log.e("recyclerview", "Current page size while going up (after): " + PAGE_SIZE);
//                    }
//                    if (PAGE_SIZE < 30 && llm.findFirstVisibleItemPosition() == 0) {
//                        PAGE_SIZE = 30;
//                        count = 0;
//                        pageNumberContainer.clearCheck();
//                        pageNumberContainer.check(count);
//                    }
//
//                }
//            }
//        });


}