package com.lipstickswatches.android.fragments;

import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lipstickswatches.android.R;
import com.lipstickswatches.android.adapter.ListsAdapter;
import com.lipstickswatches.android.db.pojo.Products;
import com.lipstickswatches.android.networks.NetworkManager;
import com.lipstickswatches.android.networks.RequestResponseListener;
import com.lipstickswatches.android.pojo.ProductResponse.Datum;
import com.lipstickswatches.android.utils.AppUtil;
import com.lipstickswatches.android.utils.ApplicationController;
import com.lipstickswatches.android.utils.Constants;
import com.lipstickswatches.android.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SearchLipsticksFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {
    SearchView svList;
    RecyclerView rvList;
    ListsAdapter listsAdapter;
    List<Products> productList;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search_lipsticks, container, false);
        return init(v);
    }

    private View init(View v) {

//        svList = v.findViewById(R.id.svList);
        svList.setOnQueryTextListener(this);

        rvList = v.findViewById(R.id.rvList);
        GridLayoutManager llm = new GridLayoutManager(getContext(), 2);
        rvList.setLayoutManager(llm);

//        srlList = v.findViewById(R.id.srlList);
//        srlList.setOnRefreshListener(this);

        if (ApplicationController.getDatabaseObj().productDAO().getAllProducts().size() < 0) {
            callService(true);
        } else {
            productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts();
            listsAdapter = new ListsAdapter(productList, getContext(), true);
            rvList.setAdapter(listsAdapter);
        }

        return v;
    }

    public void callService(boolean isProgress) {
        if (isProgress) {
            showProgress(getContext());
        }
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));

        NetworkManager.getInstance().post("get_all_products", params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
//                if (srlList.isRefreshing()) {
//                    srlList.setRefreshing(false);
//                }
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        dismissProgress();
                        AppUtil.showToast(getContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        dismissProgress();
                        AppUtil.showToast(getContext(), getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        Gson gson = new Gson();
                        ArrayList<Datum> newProductList = gson.fromJson((String) object, new TypeToken<ArrayList<Datum>>() {
                        }.getType());

                        AppUtil.removeProducts();
                        AppUtil.saveProducts(newProductList);

                        productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts();
                        listsAdapter = new ListsAdapter(productList, getContext(), true);
                        rvList.setAdapter(listsAdapter);
                        break;
                }
            }
        });


    }

    public void callServiceByBrandId(String brandId) {
        showProgress(getContext());
        Map<String, String> params = new HashMap<>();
        params.put("user_id", String.valueOf(SharedPrefUtils.readInteger("userId", 0)));
        params.put("brand_id", brandId);

        NetworkManager.getInstance().post("get_brand_products", params, new RequestResponseListener<Object>() {
            @Override
            public void onResult(Integer response, Object object) {
                switch (response) {
                    case Constants.WebApi.Response.NO_INTERNET:
                        dismissProgress();
                        AppUtil.showToast(getContext(), getString(R.string.no_internet));
                        break;
                    case Constants.WebApi.Response.NOT_FOUND:
                        dismissProgress();
                        AppUtil.showToast(getContext(), getString(R.string.no_products));
                        break;
                    case Constants.WebApi.Response.SUCCESS:
                        Gson gson = new Gson();
                        ArrayList<Datum> newProductList = gson.fromJson((String) object, new TypeToken<ArrayList<Datum>>() {
                        }.getType());

                        AppUtil.removeProducts();
                        AppUtil.saveProducts(newProductList);

                        productList = ApplicationController.getDatabaseObj().productDAO().getAllProducts();
                        dismissProgress();
                        listsAdapter = new ListsAdapter(productList, getContext(), true);
                        rvList.setAdapter(listsAdapter);
                        break;
                }
            }
        });

//        if (srlList.isRefreshing()) {
//            srlList.setRefreshing(false);
//        }
    }


    public void updateList(Products product) {
        if (productList != null && productList.size() > 0) {
            for (Products prd : productList) {
                if (prd.getId().equals(product.getId())) {
                    prd.setIsFavoriteByUser(product.getIsFavoriteByUser());
                    break;
                }

            }
            listsAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onRefresh() {
        callService(false);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listsAdapter.getFilter().filter(newText);
        return true;
    }
}
